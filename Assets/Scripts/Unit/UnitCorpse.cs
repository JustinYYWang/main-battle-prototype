using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitCorpse : MonoBehaviour
{
    [SerializeField] private Transform ragdollRootBone;

    //Initial object, getting unit position before death
    public void Setup(Transform originalRootBone)
    {
        MatchAllChildTransform(originalRootBone, ragdollRootBone);

        //The shoke wave that implement on the corpse
        Vector3 randomDir = new Vector3(Random.Range(-1f, +1f), 0 , Random.Range(-1f, +1f));
        ApplyExplosionToRagdoll(ragdollRootBone, 200f, transform.position + randomDir, 10f);
    }

    //Set the position at all parts of component
    private void MatchAllChildTransform(Transform root, Transform clone)
    {
        foreach (Transform child in root)
        {
            Transform cloneChild = clone.Find(child.name);
            if(cloneChild != null)
            {
                cloneChild.position = child.position;
                cloneChild.rotation = child.rotation;

                MatchAllChildTransform(child, cloneChild);
            }
        }
    }

    //Create a more dramatic dead scene
    private void ApplyExplosionToRagdoll (Transform root, float explosionForce, Vector3 explosionPosition, float explosionRadius)
    {
        foreach (Transform child in root)
        {
            if (child.TryGetComponent<Rigidbody>(out Rigidbody childRigidBody))
            {
                childRigidBody.AddExplosionForce(explosionForce, explosionPosition, explosionRadius);
            }

            ApplyExplosionToRagdoll(child, explosionForce, explosionPosition, explosionRadius);
        }
    }
}
