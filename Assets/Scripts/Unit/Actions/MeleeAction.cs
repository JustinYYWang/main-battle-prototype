using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeAction : BaseAction
{
    public static EventHandler OnAnyMeleeAttackHit;

    public event EventHandler OnMeleeActionStarted;
    public event EventHandler OnMeleeActionCompleted;

    private enum State
    {
        BeforeMeleeAction,
        AfterMeleeAction,
    }

    private int maxMeleeDistance = 1;
    private State state;
    private float stateTimer;
    private Unit targetUnit;

    private void Update()
    {
        if (!isActive)
        {
            return;
        }

        //Delay action to create dramatic scene. 
        stateTimer -= Time.deltaTime;

        //State machine
        switch (state)
        {
            case State.BeforeMeleeAction:
                //Rotate toward target
                Vector3 aimDir = (targetUnit.GetWorldPosition() - unit.GetWorldPosition()).normalized;

                float rotateSpeed = 10f;
                transform.forward = Vector3.Lerp(transform.forward, aimDir, rotateSpeed * Time.deltaTime);
                break;
            case State.AfterMeleeAction:
                break;
        }

        if (stateTimer <= 0f)
        {
            NextState();
        }
    }

    //State machine - determined if current state needs to advanced into next state
    private void NextState()
    {
        switch (state)
        {
            case State.BeforeMeleeAction:
                if (stateTimer <= 0f)
                {
                    state = State.AfterMeleeAction;
                    float afterHitStateTime = .5f;
                    stateTimer = afterHitStateTime;
                    OnAnyMeleeAttackHit?.Invoke(this, EventArgs.Empty);
                    targetUnit.Damage(100);
                }
                break;
            case State.AfterMeleeAction:
                OnMeleeActionCompleted?.Invoke(this, EventArgs.Empty);
                ActionComplete();
                break;
        }
    }

    public override string GetActionName()
    {
        return "Melee";
    }

    public int GetMaxDistance()
    {
        return maxMeleeDistance;
    }

    //Return melee action in enemy AI mode, always take melee action when the unit can
    public override EnemyAIAction GetEnemyAIAction(GridPosition gridPosition)
    {
        return new EnemyAIAction
        {
            gridPosition = gridPosition,
            actionValue = 200,
        };
    }

    // Calculate the grid-positions that can be interacted with this action
    public override List<GridPosition> GetValidActionGridPositions()
    {
        List<GridPosition> validGridPositionList = new List<GridPosition>();

        GridPosition unitGridPosition = unit.GetGridPosition();
        //Check every girds in the area of the action's distance if the action can be implement 
        for (int x = -maxMeleeDistance; x <= maxMeleeDistance; x++)
        {
            for (int z = -maxMeleeDistance; z <= maxMeleeDistance; z++)
            {
                //Get grid-position around current grid-position
                GridPosition offsetGridPosition = new GridPosition(x, z, 0);
                GridPosition testGridPosition = unitGridPosition + offsetGridPosition;

                if (!LevelGrid.Instance.IsValidGridPosition(testGridPosition))
                    continue;

                if (!LevelGrid.Instance.HasAnyUnitOnGridPosition(testGridPosition))
                    // Should have another unit on the selected grid-position
                    continue;

                Unit targetUnit = LevelGrid.Instance.GetUnitAtGridPosition(testGridPosition);

                //Check if the target is at the same side as cuurent turn holder
                if (targetUnit.IsEnemy() == unit.IsEnemy()) continue;

                validGridPositionList.Add(testGridPosition);
            }
        }
        return validGridPositionList;
    }

    //Initial the melee action and get the target unit
    public override void TakeAction(GridPosition gridPosition, Action onActionComplete)
    {
        targetUnit = LevelGrid.Instance.GetUnitAtGridPosition(gridPosition);

        state = State.BeforeMeleeAction;
        float beforeHitStateTime = .7f;
        stateTimer = beforeHitStateTime;

        OnMeleeActionStarted?.Invoke(this, EventArgs.Empty);

        ActionStart(onActionComplete);
    }
}
