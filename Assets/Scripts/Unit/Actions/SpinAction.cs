using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinAction : BaseAction
{
    private float totalSpinAmount;

    // Update is called once per frame
    void Update()
    {
        if (!isActive)
        {
            return;
        }
        
        float spinAddAmount = 360f * Time.deltaTime;
        transform.eulerAngles += new Vector3(0, spinAddAmount, 0);

        totalSpinAmount += spinAddAmount;
        //When action is completed
        if(totalSpinAmount >= 360f)
        {
            ActionComplete();
        }
    }

    //Spin
    public override void TakeAction(GridPosition gridPosition, Action onSpinComplete)
    {
        totalSpinAmount = 0f;

        ActionStart(onSpinComplete);
    }
    //Generic Usage(Cont.), see parent example at BaseAction
    //public class SpinBaseParameters : BaseParameters { }
    //public override void TakeAction(BaseParameters baseParameters, Action onActionComplete){
    //  SpinBaseParameters spinBaseParameters = (SpinBaseParameters)baseParameters;
    //  ...
    //}

    // Calculate the grid that can be interacted with
    public override List<GridPosition> GetValidActionGridPositions()
    {
        //Turn only in unit's grid 
        GridPosition unitGridPosition = unit.GetGridPosition();

        return new List<GridPosition>
        {
            unitGridPosition
        };
    }

    //Spin action cost 2 aps
    public override int GetAPCost()
    {
        return 1;
    }

    //Return spin action in enemy AI mode
    public override EnemyAIAction GetEnemyAIAction(GridPosition gridPosition)
    {
        return new EnemyAIAction
        {
            gridPosition = gridPosition,
            actionValue = 0,
        };
    }

    public override string GetActionName()
    {
        return "Spin";
    }
}
