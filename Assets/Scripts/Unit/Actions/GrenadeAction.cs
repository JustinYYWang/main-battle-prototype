using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GrenadeAction : BaseAction
{
    [SerializeField] private Transform grenadeProjectilePrefab;
    [SerializeField] private LineRenderer lineRenderer;
    [SerializeField] private int maxPhysicsFrameIterations = 20;
    [Header("Display Controls")]
    [SerializeField] private Transform shoulderPoint;

    public float throwForce;
    private int maxThrowDistance = 2;

    public void Update()
    {
        if (!isActive)
        {
            return;
        }
    }

    public override string GetActionName()
    {
        return "Grenade";
    }

    //When selecting grenade action, draw a projectile line target to target position
    public override void WhileSelecting()
    {
        lineRenderer.enabled = true;
        Vector3 mousePosition = MouseWorld.GetPositionOnlyHitVisible();
        GridPosition mouseGridPosition = LevelGrid.Instance.GetGridPosition(mousePosition);

        if (IsValidActionGridPosition(mouseGridPosition))
        {
            //Debug.Log(mouseGridPosition);
            DrawProjection(mousePosition);
            rotateThrowingPoint(mousePosition);
        }

    }

    //When changing grenade action into other, undraw line renderer
    public override void ChangeSelecting()
    {
        lineRenderer.enabled = false;
    }

    private void DrawProjection(Vector3 targetPosition)
    {
        var ghostObj = Instantiate(grenadeProjectilePrefab, shoulderPoint.position, Quaternion.identity);
        SceneManager.MoveGameObjectToScene(ghostObj.gameObject, Simulator.Instance._simulationScene);

        GrenadeProjectile ghostGrenadeProjectile = ghostObj.GetComponent<GrenadeProjectile>();
        ghostGrenadeProjectile.Setup(null);
        ghostGrenadeProjectile.GetComponent<Rigidbody>().linearVelocity = throwForce * shoulderPoint.forward;

        lineRenderer.positionCount = maxPhysicsFrameIterations;
        for (int i = 0; i < maxPhysicsFrameIterations; i++)
        {
            Simulator.Instance._physicsScene.Simulate(Time.fixedDeltaTime);
            lineRenderer.SetPosition(i, ghostObj.transform.position);
        }

        Destroy(ghostObj.gameObject);
    }

    //Return grenade action in enemy AI mode, //NEED TO BE MODIFIED//
    public override EnemyAIAction GetEnemyAIAction(GridPosition gridPosition)
    {
        return new EnemyAIAction
        {
            gridPosition = gridPosition,
            actionValue = 0,
        };
    }

    //Check every girds in the area of the action's distance if the action can be implement 
    //Take shootaction for reference, as for validate range bug
    public override List<GridPosition> GetValidActionGridPositions()
    {
        List<GridPosition> validGridPositionList = new List<GridPosition>();

        GridPosition unitGridPosition = unit.GetGridPosition();
        //Get the can-throw area of the grids
        for (int x = -maxThrowDistance; x <= maxThrowDistance; x++)
        {
            for (int z = -maxThrowDistance; z <= maxThrowDistance; z++)
            {
                for (int floor = -maxThrowDistance; floor <= maxThrowDistance; floor++)
                {
                    //Get grid-position around current grid-position
                    GridPosition offsetGridPosition = new GridPosition(x, z, floor);
                    GridPosition testGridPosition = unitGridPosition + offsetGridPosition;

                    if (!LevelGrid.Instance.IsValidGridPosition(testGridPosition))
                        continue;

                    //Circle throwing area
                    int throwDistance = Mathf.Abs(x) + Mathf.Abs(z);
                    //Restrict the throwing area
                    if (throwDistance > maxThrowDistance) continue;

                    validGridPositionList.Add(testGridPosition);
                }
            }
        }
        return validGridPositionList;
    }

    //Initial the grenade action and the grenade object
    public override void TakeAction(GridPosition gridPosition, Action onActionComplete)
    {
        Vector3 targetPosition = LevelGrid.Instance.GetWorldPosition(gridPosition);
        lineRenderer.enabled = false;

        Transform grenadeProjectileTransform = Instantiate(grenadeProjectilePrefab, shoulderPoint.position, Quaternion.identity);
        GrenadeProjectile grenadeProjectile = grenadeProjectileTransform.GetComponent<GrenadeProjectile>();
        grenadeProjectile.Setup(onGrenadeBehaviourComplete);
        grenadeProjectileTransform.GetComponent<Rigidbody>().linearVelocity = throwForce * shoulderPoint.forward;

        ActionStart(onActionComplete);
    }

    //throwing change to fix angle but adjustable force?
    //The calculateangle function can be remained
    void rotateThrowingPoint(Vector3 targetPosition)
    {
        float? throwingAngle = CalculateAngle(false, targetPosition);
        Debug.Log(throwingAngle);

        Vector3 aimDir = (targetPosition - unit.transform.position).normalized;
        aimDir.y = 0f;
        Quaternion rotationGoal = Quaternion.LookRotation(aimDir);
        unit.transform.rotation = rotationGoal;

        if (throwingAngle != null)
            shoulderPoint.localEulerAngles = new Vector3(360f - (float)throwingAngle, 0, 0);
    } 

    //Use tan theta equation to find angle
    //Adjust throw force?
    float? CalculateAngle(bool low, Vector3 targetPosition)
    {
        Vector3 targetDir = targetPosition - shoulderPoint.position;
        float y = targetDir.y;
        targetDir.y = 0;
        float x = targetDir.magnitude;
        float gravity = 9.8f;
        float speedSqr = throwForce * throwForce;

        float underSquareRoot = (speedSqr * speedSqr) - gravity * (gravity * x * x + 2 * y * speedSqr);
        if (underSquareRoot >= 0f)
        {
            float root = Mathf.Sqrt(underSquareRoot);
            float highAngle = speedSqr + root;
            float lowAngle = speedSqr - root;

            if (low)
                return (Mathf.Atan2(lowAngle, gravity * x) * Mathf.Rad2Deg);
            else
                return (Mathf.Atan2(highAngle, gravity * x) * Mathf.Rad2Deg);
        }
        else
            return null;

    }

    //Call the function when grenade projectile finish
    private void onGrenadeBehaviourComplete()
    {
        ActionComplete();
    }
}
