using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseAction : MonoBehaviour
{
    public static event EventHandler OnAnyActionStarted;
    public static event EventHandler OnAnyActionCompleted;

    protected Unit unit;
    protected bool isActive;
    protected Action onActionComplete;


    protected virtual void Awake()
    {
        unit = GetComponent<Unit>();
    }

    public abstract String GetActionName();

    //When action has been selected
    public virtual void WhileSelecting() { }

    //When action has been changed
    public virtual void ChangeSelecting() { }

    public abstract void TakeAction(GridPosition gridPosition, Action onActionComplete);

    //Set complete action function when the action is started
    //Make sure it has be called after action is ready
    protected void ActionStart(Action onActionComplete)
    {
        isActive = true;
        this.onActionComplete = onActionComplete;

        OnAnyActionStarted?.Invoke(this, EventArgs.Empty);
    }

    //Complete action, do passed function
    //Make sure it has be called after action is complete
    protected void ActionComplete()
    {
        isActive = false;
        onActionComplete();

        OnAnyActionCompleted?.Invoke(this, EventArgs.Empty);
    }

    // Check if the grid is in the can-action grid
    public virtual bool IsValidActionGridPosition(GridPosition gridPosition)
    {
        List<GridPosition> validGridPositionList = GetValidActionGridPositions();
        return validGridPositionList.Contains(gridPosition);
    }

    // Calculate the grid that the action can be interacted with
    public abstract List<GridPosition> GetValidActionGridPositions();

    //Generic Usage, see child example at SpinAction
    //public class BaseParameters { }
    //public abstract void TakeAction(BaseParameters baseParameters, Action onActionComplete);

    //Define and return the cost of the action points for taking this action
    public virtual int GetAPCost()
    {
        return 1;
    }

    public Unit GetUnit()
    {
        return unit;
    }

    //Cycle through all enemy-AI-actions with valid grid-positions, and choose the most value one
    public EnemyAIAction GetBestEnemyAIAction()
    {
        List<EnemyAIAction> enemyAIActions = new List<EnemyAIAction>();

        List<GridPosition> validActionGridPositions =  GetValidActionGridPositions();

        foreach (GridPosition gridPosition in validActionGridPositions)
        {
            EnemyAIAction enemyAIAction = GetEnemyAIAction(gridPosition);
            enemyAIActions.Add(enemyAIAction);
        }

        if(enemyAIActions.Count > 0)
        {
            enemyAIActions.Sort((EnemyAIAction a, EnemyAIAction b) => b.actionValue - a.actionValue);
            return enemyAIActions[0];
        }
        else
        {
            //No possible Enemy AI Actions
            return null;
        }
    }

    //Each action should have its own enemy AI mode
    public abstract EnemyAIAction GetEnemyAIAction(GridPosition gridPosition);
}
