using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MoveAction : BaseAction
{
    public event EventHandler OnStartMoving;
    public event EventHandler OnStopMoving;
    public event EventHandler<OnChangedFloorArgs> OnChangedFloor;
    public class OnChangedFloorArgs : EventArgs
    {
        public GridPosition unitGridPosition;
        public GridPosition targetGridPosition;
    }

    private List<Vector3> positions;
    private int currentPositionIndex;
    private bool isChangingFloors;
    private float floorTransitionTime;
    private float floorTransitionMaxTime = .5f;

    [SerializeField] private int maxMoveDistance = 4;
    [SerializeField] private float stoppingDistance = .1f;

    float rotateSpeed = 5f;
    Quaternion rotationGoal;

    // Update is called once per frame
    void Update()
    {
        if (!isActive)
        {
            return;
        }


        Vector3 targetPosition = positions[currentPositionIndex];

        if (isChangingFloors)
        {
            //Start changing floors
            //Wait until the animation is done, then logicaly transmitted the target position
            Vector3 targetHorizontalPosition = targetPosition;
            targetHorizontalPosition.y = transform.position.y;
            //Lock unit for rotating along y-axis
            Vector3 moveDirection = (targetHorizontalPosition - transform.position).normalized;
            //Rotate toward target
            rotationGoal = Quaternion.LookRotation(moveDirection);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotationGoal, rotateSpeed * Time.deltaTime);

            floorTransitionTime -= Time.deltaTime;

            if (floorTransitionTime < 0f)
            {
                isChangingFloors = false;
                transform.position = targetPosition;
            }
        }
        else
        {
            //Start regular move 
            Vector3 moveDirection = (targetPosition - transform.position).normalized;

            //Rotate toward target
            rotationGoal = Quaternion.LookRotation(moveDirection);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotationGoal, rotateSpeed * Time.deltaTime);

            float moveSpeed = 4f;
            transform.position += moveDirection * moveSpeed * Time.deltaTime;
        }

        //Unit Movement Complete
        if (Vector3.Distance(transform.position, targetPosition) <= stoppingDistance)
        {
            currentPositionIndex++;
            if(currentPositionIndex >= positions.Count)
            {
                OnStopMoving?.Invoke(this, EventArgs.Empty);

                ActionComplete();
            }
            else
            {
                targetPosition = positions[currentPositionIndex];
                GridPosition targetGridPosition = LevelGrid.Instance.GetGridPosition(targetPosition);
                GridPosition unitGridPosition = LevelGrid.Instance.GetGridPosition(transform.position);

                //Unit is at its destination or at the edge
                if (targetGridPosition.floor != unitGridPosition.floor)
                {
                    //Different floors
                    isChangingFloors = true;
                    floorTransitionTime = floorTransitionMaxTime;

                    OnChangedFloor.Invoke(this, new OnChangedFloorArgs
                    {
                        unitGridPosition = unitGridPosition,
                        targetGridPosition = targetGridPosition,
                    });
                }
            }
        }
    }

    //Move by pathfinding algorithm
    public override void TakeAction(GridPosition gridPosition, Action onMoveComplete)
    {
       List<GridPosition> pathGridPositions = PathFinding.Instance.FindPath(unit.GetGridPosition(), gridPosition, out int pathLength);

        currentPositionIndex = 0;
        positions = new List<Vector3>();

        foreach (GridPosition pathGridPosition in pathGridPositions)
        {
            positions.Add(LevelGrid.Instance.GetWorldPosition(pathGridPosition));
        }

        OnStartMoving?.Invoke(this, EventArgs.Empty);

        ActionStart(onMoveComplete);
    }

    //Check every girds in the area of the action's distance if the action can be implement 
    public override List<GridPosition> GetValidActionGridPositions()
    {
        List<GridPosition> validGridPositionList = new List<GridPosition>();

        GridPosition unitGridPosition = unit.GetGridPosition();

        //Get the can-move area of the grids
        for (int x = -maxMoveDistance; x <= maxMoveDistance; x++)
        {
            for(int z = -maxMoveDistance; z <= maxMoveDistance; z++)
            {
                for(int floor = -maxMoveDistance; floor <= maxMoveDistance; floor++)
                {
                    GridPosition offsetGridPosition = new GridPosition(x, z, floor);
                    GridPosition testGridPosition = unitGridPosition + offsetGridPosition;

                    if (!LevelGrid.Instance.IsValidGridPosition(testGridPosition))
                        continue;

                    if (unitGridPosition == testGridPosition)
                        // Same Grid Position where the unit is already at
                        continue;

                    if (LevelGrid.Instance.HasAnyUnitOnGridPosition(testGridPosition))
                        // Grid Position already occupied with another unit
                        continue;

                    if (!PathFinding.Instance.IsWalkableGridPosition(testGridPosition))
                        //Grid Position is not walkable
                        continue;

                    if (!PathFinding.Instance.HasPath(unitGridPosition, testGridPosition))
                        //There is no path to reach the grid position
                        continue;

                    int pathfindingDistanceMultipier = 10;
                    if (PathFinding.Instance.GetPathLength(unitGridPosition, testGridPosition) > maxMoveDistance * pathfindingDistanceMultipier)
                    {
                        //Path is too long
                        continue;
                    }

                    validGridPositionList.Add(testGridPosition);

                }
            }
        }

        return validGridPositionList;
    }

    //Return move action in enemy AI mode
    public override EnemyAIAction GetEnemyAIAction(GridPosition gridPosition)
    {
        int targetCountAtGridPosition = unit.GetAction<ShootAction>().GetTargetCountAtPosition(gridPosition);
        //Move to the position where the most target can shoot to 
        return new EnemyAIAction
        {
            gridPosition = gridPosition,
            actionValue = targetCountAtGridPosition * 10,
        };
    }

    public override string GetActionName()
    {
        return "Move";
    }
}
