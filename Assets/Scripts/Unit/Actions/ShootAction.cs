using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ShootAction : BaseAction
{
    public static event EventHandler<ShootingArgs> OnAnyShoot;
    public event EventHandler<ShootingArgs> OnShoot;

    //Self defined eventargs
    public class ShootingArgs : EventArgs
    {
        public Unit targetUnit;
        public Unit shootingUnit;
        public CoverType targetCoverType;
    }

    private enum ShootingState
    {
        Aiming,
        Shooting,
        Cooloff,
    }

    [SerializeField] private LayerMask obstacleLayerMask;
    private ShootingState shootingState;
    private int maxShootDistance = 7;
    private float stateTimer;
    private Unit targetUnit;

    private bool canShootBullet;

    void Update()
    {
        if (!isActive)
        {
            return;
        }

        //Delay action to create dramatic scene. 
        stateTimer -= Time.deltaTime;

        //State machine
        switch (shootingState)
        {
            case ShootingState.Aiming:
                //Rotate toward target
                Vector3 aimDir = (targetUnit.GetWorldPosition() - unit.GetWorldPosition()).normalized;
                aimDir.y = 0f;

                float rotateSpeed = 5f;
                Quaternion rotationGoal = Quaternion.LookRotation(aimDir);
                transform.rotation = Quaternion.Slerp(transform.rotation, rotationGoal, rotateSpeed * Time.deltaTime);
                break;
            case ShootingState.Shooting:
                if (canShootBullet)
                {
                    Shoot();
                    canShootBullet = false;
                }
                break;
            case ShootingState.Cooloff:
                break;
        }

        if (stateTimer <= 0f)
        {
            NextState();
        }
    }

    //State machine - determined if current state needs to advanced into next state
    private void NextState()
    {
        switch (shootingState)
        {
            case ShootingState.Aiming:
                shootingState = ShootingState.Shooting;
                float shootingStateTime = .1f;
                stateTimer = shootingStateTime;
                break;
            case ShootingState.Shooting:
                shootingState = ShootingState.Cooloff;
                float cooloffStateTime = .5f;
                stateTimer = cooloffStateTime;
                break;
            case ShootingState.Cooloff:
                ActionComplete();
                break;
        }
    }

    // Calculate the grid-positions that can be interacted with this action
    public override List<GridPosition> GetValidActionGridPositions()
    {
        GridPosition unitGridPosition = unit.GetGridPosition();
        return GetValidActionGridPositions(unitGridPosition);
    }

    //Check every girds in the area of the action's distance if the action can be implement 
    public List<GridPosition> GetValidActionGridPositions(GridPosition unitGridPosition)
    {
        List<GridPosition> validGridPositionList = new List<GridPosition>();
        
        //Get the can-shoot area of the grids
        for (int x = -maxShootDistance; x <= maxShootDistance; x++)
        {
            for (int z = -maxShootDistance; z <= maxShootDistance; z++)
            {
                for (int floor = -maxShootDistance; floor <= maxShootDistance; floor++)
                {
                    //Get grid-position around current grid-position
                    GridPosition offsetGridPosition = new GridPosition(x, z, floor);
                    GridPosition tempGridPosition = unitGridPosition + offsetGridPosition;

                    if (!LevelGrid.Instance.IsValidGridPosition(tempGridPosition))
                        continue;

                    //Circle shooting area
                    int shootDistance = Mathf.Abs(x) + Mathf.Abs(z);
                    //Restrict the shooting area
                    if (shootDistance > maxShootDistance) continue;

                    if (!LevelGrid.Instance.HasAnyUnitOnGridPosition(tempGridPosition))
                        // Should have another unit on the selected grid-position
                        continue;

                    Unit targetUnit = LevelGrid.Instance.GetUnitAtGridPosition(tempGridPosition);

                    //Check if the target is at the same side as cuurent turn holder
                    if (targetUnit.IsEnemy() == unit.IsEnemy()) continue;

                    //Check if the unit can see the target
                    Vector3 unitWorldPosition = LevelGrid.Instance.GetWorldPosition(unitGridPosition);
                    Vector3 shootDir = (targetUnit.GetWorldPosition() - unitWorldPosition).normalized;
                    if (Physics.Raycast(unitWorldPosition + Vector3.up * Properties.unitShoulderHeight,// <- be caution for this way of approching
                        shootDir,
                        Vector3.Distance(unitWorldPosition, targetUnit.GetWorldPosition()),
                        obstacleLayerMask
                        ))
                    {
                        // Blocked by an obstacles
                        continue;
                    }

                    validGridPositionList.Add(tempGridPosition);
                }
            }
        }
        return validGridPositionList;
    }

    //Check if target is behind the cover
    public CoverType CheckTargetCover(Unit targetUnit)
    {
        //Fetch all objects through the target
        float objectOffset = .1f;
        Vector3 unitWorldPosition = unit.GetWorldPosition();
        Vector3 shootDir = (targetUnit.GetWorldPosition() - unitWorldPosition).normalized;
        RaycastHit[] hits = Physics.RaycastAll(unitWorldPosition + Vector3.up * objectOffset, 
            shootDir,
            Vector3.Distance(unitWorldPosition, targetUnit.GetWorldPosition()),
            obstacleLayerMask);

        //Check if the object is coverable
        foreach (RaycastHit hit in hits )
        {
            if (hit.transform.TryGetComponent(out ICoverable coverableObject))
            {
                //Check if the object is providing coverage to target unit
                List<GridObject> adjacencyGridObjects = LevelGrid.Instance.GetAdjacencyGridObjects(coverableObject.gridPosition);
                foreach (GridObject adjacencyGridObject in adjacencyGridObjects)
                {
                    if (targetUnit == adjacencyGridObject.GetUnit())
                        return coverableObject.cover;
                }
            }
        }
        return CoverType.None;
    }

    //Initial the shoot action and get the target unit
    public override void TakeAction(GridPosition gridPosition, Action onShootComplete)
    {
        targetUnit = LevelGrid.Instance.GetUnitAtGridPosition(gridPosition);

        shootingState = ShootingState.Aiming;
        float aimingStateTime = 1f;
        stateTimer = aimingStateTime;

        canShootBullet = true;

        ActionStart(onShootComplete);
    }

    //Shoot action logical and animation implement
    private void Shoot()
    {
        float damageAmount;
        
        ShootingArgs shootingArgs = new ShootingArgs
        {
            targetUnit = targetUnit,
            shootingUnit = unit
        };

        OnAnyShoot?.Invoke(this, shootingArgs);
        OnShoot?.Invoke(this, shootingArgs);

        shootingArgs.targetCoverType = CheckTargetCover(targetUnit);
        damageAmount = GameModifier.damageModifier(40, shootingArgs);
        targetUnit.Damage(damageAmount);
    }
    
    public Unit GetTargetUnit()
    {
        return targetUnit;
    }

    public int GetMaxDistance()
    {
        return maxShootDistance;
    }

    //Return shoot action in enemy AI mode, shoot the weekest unit
    public override EnemyAIAction GetEnemyAIAction(GridPosition gridPosition)
    {
        Unit targetUnit = LevelGrid.Instance.GetUnitAtGridPosition(gridPosition);

        return new EnemyAIAction
        {
            gridPosition = gridPosition,
            actionValue = 100 + Mathf.RoundToInt((1 - targetUnit.GetHealthNormalized()) * 100f),
        };
    }

    //Get the number of grids that can shoot to, in another word,
    //how many target can be shoot when unit is on the grid-position
    public int GetTargetCountAtPosition(GridPosition gridPosition)
    {
        return GetValidActionGridPositions(gridPosition).Count;
    }

    public override string GetActionName()
    {
        return "Shoot";
    }
}
