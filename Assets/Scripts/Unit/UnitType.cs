using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="UnitType", menuName ="ScriptableObjects/Unit/Type")]
public class UnitType : ScriptableObject
{
   public int MaxHealth;
   public int MaxActionPoints;
}
