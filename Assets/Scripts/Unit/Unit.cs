//Unit Script has to initilize after objects and grid status has fully set 
using System;
using Tatooine;
using UnityEngine;

public class Unit : MonoBehaviour, IHitable
{
    public static event EventHandler OnAnyActionPointsChanged;
    public static event EventHandler OnAnyUnitSpawned;
    public static event EventHandler OnAnyUnitDead;

    [SerializeField] private bool isEnemy;

    private GridPosition unitGridPosition;
    private HealthSystem healthSystem;
    private UnitStatus unitController;
    private BaseAction[] baseActions;
    //Action Points refilled to max every turn for now
    [SerializeField] private int actionPoints;

    public event EventHandler OnChangeCoverStatus;
    private bool underCover;

    public void Awake()
    {
        unitController = GetComponent<UnitStatus>();
        healthSystem = GetComponent<HealthSystem>();
        baseActions = GetComponents<BaseAction>();
    }

    private void Start()
    {
        actionPoints = unitController.MaxActionPoints;
        unitGridPosition = LevelGrid.Instance.GetGridPosition(transform.position);
        LevelGrid.Instance.AddUnitAtGridPosition(unitGridPosition, this);

        //Check unit is under cover and invoke animation
        underCover = LevelGrid.Instance.beingCover(unitGridPosition);
        if (underCover)
            OnChangeCoverStatus?.Invoke(this, EventArgs.Empty);

        TurnSystem.Instance.OnTurnChanged += TurnSystem_OnTurnChanged;
        healthSystem.OnDead += HealthSystem_OnDead;
 
        OnAnyUnitSpawned?.Invoke(this, EventArgs.Empty);
    }

    private void Update()
    {
        GridPosition currentGridPosition = LevelGrid.Instance.GetGridPosition(transform.position);
        if (currentGridPosition != unitGridPosition)
        {
            // Unit changed grid position
            GridPosition oldGridPosition = unitGridPosition;
            unitGridPosition = currentGridPosition;

            LevelGrid.Instance.UnitMovedGridPosition(this, oldGridPosition, currentGridPosition);
            bool underCover = LevelGrid.Instance.beingCover(currentGridPosition);
            SetUnitUnderCover(underCover);
        }
    }

    //Get generic Action
    public T GetAction<T>() where T : BaseAction
    {
        foreach (BaseAction baseAction in baseActions)
        {
            if (baseAction is T)
            {
                return (T)baseAction;
            }
        }
        return null;
    }

    public GridPosition GetGridPosition()
    {
        return unitGridPosition;
    }

    public Vector3 GetWorldPosition()
    {
        return transform.position;
    }

    public BaseAction[] GetBaseActions()
    {
        return baseActions;
    }

    //Check action points is enough to take the action
    public bool CanTakeAPsAction(BaseAction baseAction)
    {
        return actionPoints >= baseAction.GetAPCost();
    }

    //Spend action points
    public void SpendActionPoints(int amount)
    {
        actionPoints -= amount;

        OnAnyActionPointsChanged?.Invoke(this, EventArgs.Empty);
    }

    //Spend action points if action points is enough to take the action.
    public bool TryTakeAPsAction(BaseAction baseAction)
    {
        if (CanTakeAPsAction(baseAction))
        {
            SpendActionPoints(baseAction.GetAPCost());
            return true;
        }
        else
        {
            return false;
        }
    }

    //Handle event OnTurnChanged from TurnSystem script, refresh units status every time current turn has changed
    private void TurnSystem_OnTurnChanged(object sender, EventArgs e)
    {
        //Only reset enemy's action points when is enemy turn or reset unit action points when is player's turn
        if ((IsEnemy() && !TurnSystem.Instance.IsPlayerTurn()) || 
            (!IsEnemy() && TurnSystem.Instance.IsPlayerTurn()))
        {
            actionPoints = unitController.MaxActionPoints;

            OnAnyActionPointsChanged?.Invoke(this, EventArgs.Empty);
        }
    }

    //Handle event OnDead from TurnSystem script, destroy gameobject and data that is reamin on the grid and fire OnAnyUnitDead event when this unit is dead
    private void HealthSystem_OnDead(object sender, EventArgs e)
    {
        LevelGrid.Instance.RemoveUnitAtGridPosition(unitGridPosition, this);
        Destroy(gameObject);

        OnAnyUnitDead?.Invoke(this, EventArgs.Empty);
    }

    //Unit remain action points.
    public int GetActionPoints()
    {
        return actionPoints;
    }

    public bool IsEnemy()
    {
        return isEnemy;
    }

    //Set Unit is under cover
    public void SetUnitUnderCover(bool isCover)
    {
        if(underCover != isCover)
        {
            underCover = isCover;
            OnChangeCoverStatus?.Invoke(this, EventArgs.Empty);
        }
    }

    //Damage unit by calling its health system.
    public void Damage(float damageAmount)
    {
        healthSystem.Damage(damageAmount);
    }

    //Return unit's normalized health point
    public float GetHealthNormalized()
    {
        return healthSystem.GetHealthNormalized();
    }
}
