using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Specified for project: Tatooine
namespace Tatooine
{
    //Change Status into actual points and handle scriptable objects
    public class UnitStatus : MonoBehaviour
    {
        [SerializeField] private UnitType unitType;
        [HideInInspector] public int MaxHealth;
        [HideInInspector] public int MaxActionPoints;

        //TODO: Action Conntroller
        private BaseAction[] baseActions;

        void Awake()
        {
            MaxHealth = unitType.MaxHealth;
            MaxActionPoints = unitType.MaxActionPoints;
        }
    }
}
