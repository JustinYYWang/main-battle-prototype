using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    private enum State
    {
        WaitingForEnemyTurn,
        TakingTurn,
        Busy,

    }

    private State state;
    private float timer;

    private void Awake()
    {
        state = State.WaitingForEnemyTurn;
    }

    private void Start()
    {
        TurnSystem.Instance.OnTurnChanged += TurnSystem_OnTurnChanged;
    }

    // Update is called once per frame
    void Update()
    {
        //If is player turn, do nothing here
        if (TurnSystem.Instance.IsPlayerTurn()) return;

        switch (state)
        {
            case State.WaitingForEnemyTurn:
                break;
            case State.TakingTurn:
                timer -= Time.deltaTime;
                if (timer <= 0f)
                {
                    if (EnemyAI_TryTakeAction(SetStateTakingTurn))
                        state = State.Busy;
                    //No more enemy units have actions to take, enemy turn end
                    else
                        TurnSystem.Instance.NextTurn();
                }
                break;
            case State.Busy:
                break;
        }
    }

    //Set AI turn to taking turn and reset timer
    private void SetStateTakingTurn()
    {
        timer = .5f;
        state = State.TakingTurn;
    }

    //Handle event OnTurnChanged from TurnSystem script, update timer every time current turn has changed
    private void TurnSystem_OnTurnChanged(object sender, EventArgs e)
    {
        if (!TurnSystem.Instance.IsPlayerTurn())
        {
            state = State.TakingTurn;
            timer = 2f;
        }
    }

    //Try to make every enemy units to do action
    private bool EnemyAI_TryTakeAction(Action onEnemyAI_ActionComplete)
    {
        foreach (Unit enemyUnit in UnitManager.Instance.GetEnemyUnitList())
        {
            if(EnemyAI_TryTakeAction(enemyUnit, onEnemyAI_ActionComplete))
                return true;
        }

        return false;
    }

    //Try to find the best action that is affordable and unit can do the action
    private bool EnemyAI_TryTakeAction(Unit enemyUnit, Action onEnemyAI_ActionComplete)
    {
        EnemyAIAction bestEnemyAIAction = null;
        BaseAction bestBaseAction = null;

        //Check if enemy unit has enough APs to make the best action 
        foreach (BaseAction baseAction in enemyUnit.GetBaseActions())
        {
            if (!enemyUnit.CanTakeAPsAction(baseAction))
            {
                //Enemy unit cannot afford the action
                continue;
            }

            if (bestEnemyAIAction == null)
            {
                bestEnemyAIAction = baseAction.GetBestEnemyAIAction();
                bestBaseAction = baseAction;
            }
            else
            {
                EnemyAIAction tempEnemyAIAction = baseAction.GetBestEnemyAIAction();
                if (tempEnemyAIAction != null && tempEnemyAIAction.actionValue > bestEnemyAIAction.actionValue)
                {
                    bestEnemyAIAction = tempEnemyAIAction;
                    bestBaseAction = baseAction;
                }
            }
        }

        if (bestEnemyAIAction != null && enemyUnit.TryTakeAPsAction(bestBaseAction))
        {
            bestBaseAction.TakeAction(bestEnemyAIAction.gridPosition, onEnemyAI_ActionComplete);
            return true;
        }
        else
            return false;
    }
}
