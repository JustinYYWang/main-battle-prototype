/*******************************************************************************************************
Struct    : GridSystemVisual (SINGLETON)
Author    : Justin Wang based on Hugo Cardoso(Code Monkey)
Date      : 10/12/2022-
Purpose   : Enum for colors, struct for control colors with material and functions for visualizing grids
Usage     : NULL
Memo      : The mapping of the color and materials should be done with the inspector
********************************************************************************************************/
using System;
using System.Collections.Generic;
using UnityEngine;

public class GridSystemVisual : MonoBehaviour
{
    public static GridSystemVisual Instance { get; private set; }

    //Self-defined a type that handle both material and grid-visual type
    [Serializable]
    public struct GridVisualTypeMaterial
    {
        public GridVisualType gridVisualType;
        public Material material;
    }

    //Grid visual colors
    public enum GridVisualType
    {
        White,
        Blue,
        Red,
        RedSoft,
        Yellow
    }

    [SerializeField] private Transform gridSystemVisualSinglePrefab;

    //Set Grid-Visual types and materials map at inspector
    [SerializeField] private List<GridVisualTypeMaterial> gridVisualTypes;

    private GridSystemVisualController[,,] gridSystemVisualControllers;

    private void Awake()
    {
        if (Instance != null)
        {
            Debug.LogError("There's more than one Grid System Visual!" + transform + " - " + Instance);
            Destroy(gameObject);
            return;
        }
        Instance = this;
    }

    //Set up visualized grid prefab, subscribe to event and then show grid visaulized
    private void Start()
    {
        gridSystemVisualControllers = new GridSystemVisualController[
            LevelGrid.Instance.GetWidth(),
            LevelGrid.Instance.GetHeight(),
            LevelGrid.Instance.GetFloorAmount()
        ];

        //Get all controllers from every girds
        for (int x = 0; x < LevelGrid.Instance.GetWidth(); x++)
        {
            for (int z = 0; z < LevelGrid.Instance.GetHeight(); z++)
            {
                for (int floor = 0; floor < LevelGrid.Instance.GetFloorAmount(); floor++)
                {
                    GridPosition gridPosition = new GridPosition(x, z, floor);

                    Transform gridSystemVisualSingleTransform =
                        Instantiate(gridSystemVisualSinglePrefab, LevelGrid.Instance.GetWorldPosition(gridPosition), Quaternion.identity, transform);

                    gridSystemVisualControllers[x, z, floor] = gridSystemVisualSingleTransform.GetComponent<GridSystemVisualController>();
                }
            }
        }

        UnitActionSystem.Instance.OnSelectedActionChanged += UnitActionSystem_OnSelectedActionChanged;
        UnitActionSystem.Instance.OnBusyChanged += UnitActionSystem_OnBusyChanged;
        //LevelGrid.Instance.OnAnyUnitMovedGridPosition += LevelGrid_OnAnyUnitMovedGridPosition;

        UpdateGridVisual();
    }

    //Hide all grid visualized by grid-position 
    public void HideAllGridPosition()
    {
        for (int x = 0; x < LevelGrid.Instance.GetWidth(); x++)
        {
            for (int z = 0; z < LevelGrid.Instance.GetHeight(); z++)
            {
                for (int floor = 0; floor < LevelGrid.Instance.GetFloorAmount(); floor++)
                {
                    gridSystemVisualControllers[x, z, floor].Hide();
                }
            }
        }

    }

    //Visualized the given range of grid-position with circle shape
    public void ShowGridPositionCircleRange(GridPosition gridPosition, int range, GridVisualType gridVisualType)
    {
        List<GridPosition> gridPositionList = new List<GridPosition>();
        for (int x = -range;  x <= range; x++)
        {
           for(int z = -range; z <= range; z++)
            {
                GridPosition tempGridPosition = gridPosition + new GridPosition(x, z, 0);

                if (!LevelGrid.Instance.IsValidGridPosition(tempGridPosition))
                    continue;

                int rangeDistance = Mathf.Abs(x) + Mathf.Abs(z);

                //Restrict the given range area
                if (rangeDistance > range) 
                    continue;

                gridPositionList.Add(tempGridPosition);
            }
        }

        ShowGridPositionList(gridPositionList, gridVisualType);
    }

    //Visualized the given range of grid-position with square shape
    public void ShowGridPositionSquareRange(GridPosition gridPosition, int range, GridVisualType gridVisualType)
    {
        List<GridPosition> gridPositionList = new List<GridPosition>();
        for (int x = -range; x <= range; x++)
        {
            for (int z = -range; z <= range; z++)
            {
                GridPosition tempGridPosition = gridPosition + new GridPosition(x, z, 0);

                if (!LevelGrid.Instance.IsValidGridPosition(tempGridPosition))
                    continue;

                gridPositionList.Add(tempGridPosition);
            }
        }

        ShowGridPositionList(gridPositionList, gridVisualType);
    }

    //Visualized the given list of grid-position
    public void ShowGridPositionList(List<GridPosition> gridPositionsList, GridVisualType gridVisualType)
    {
        foreach (GridPosition gridPosition in gridPositionsList)
        {
            gridSystemVisualControllers[gridPosition.x, gridPosition.z, gridPosition.floor].Show(GetGridVisualTypeMaterial(gridVisualType));
        }
    }

    //Un and revisualized grids
    private void UpdateGridVisual()
    {
        HideAllGridPosition();

        Unit selectedUnit = UnitActionSystem.Instance.GetSelectedAction().GetUnit();
        BaseAction selectedAction = UnitActionSystem.Instance.GetSelectedAction();
        GridVisualType gridVisualType;
        //Different action, different color to visualized
        switch (selectedAction)
        {
            default:
            case MoveAction moveAction:
                gridVisualType = GridVisualType.White;
                break;

            case SpinAction spinAction:
                gridVisualType = GridVisualType.Blue;
                break;

            case ShootAction shootAction:
                gridVisualType = GridVisualType.Red;

                ShowGridPositionCircleRange(selectedUnit.GetGridPosition(), shootAction.GetMaxDistance(), GridVisualType.RedSoft);
                break;

            case GrenadeAction grenadeAction:
                gridVisualType = GridVisualType.Yellow;
                break;

            case MeleeAction meleeAction:
                gridVisualType = GridVisualType.Red;

                ShowGridPositionSquareRange(selectedUnit.GetGridPosition(), meleeAction.GetMaxDistance(), GridVisualType.RedSoft);
                break;

            case InteractAction interactAction:
                gridVisualType = GridVisualType.Blue;
                break;
        }

        ShowGridPositionList(
            selectedAction.GetValidActionGridPositions(), gridVisualType);
    }

    //Handle event OnSelectedActionChanged from UnitActionSystem script, revisualized grids every time player switch action
    private void UnitActionSystem_OnSelectedActionChanged(object sender, EventArgs e)
    {
        UpdateGridVisual();
    }

    private void UnitActionSystem_OnBusyChanged(object sender, bool e)
    {
        UpdateGridVisual();
    }

    //Handle event OnAnyUnitMovedGridPosition from LevelGrid script, revisualized grids every time player grid-moved
    private void LevelGrid_OnAnyUnitMovedGridPosition(object sender, EventArgs e)
    {
        UpdateGridVisual();
    }

    //Return the corresponding material from mapping list
    private Material GetGridVisualTypeMaterial(GridVisualType gridVisualType)
    {
        foreach (GridVisualTypeMaterial gridVisualTypeMaterial in gridVisualTypes)
        {
            if (gridVisualTypeMaterial.gridVisualType == gridVisualType)
                return gridVisualTypeMaterial.material;
        }

        Debug.LogError("Could not find corresponding grid-visual type" + gridVisualType + "from the grid-visual type and material mapping table.");
        return null;
    }
}
