﻿/*******************************************************************************************************
Class     : GridObject
Author    : Justin Wang based on Hugo Cardoso(Code Monkey)
Date      : 10/12/2022-
Purpose   : Implement functions for single grid aspect problems
Usage     : NULL
Memo      : Level Grid ⊆ Grid Systems ⊆ Grid Objects
********************************************************************************************************/

using System.Collections.Generic;
using System.Diagnostics;

public class GridObject 
{
    private GridSystem<GridObject> gridSystem;
    public GridPosition gridPosition;
    private List<Unit> units;
    private IInteractable interactableObject;
    private ICoverable coverableObject;
    private List<CoverType> coverTypes;
    private bool isValid;

    public GridObject(GridSystem<GridObject> gridSystem, GridPosition gridPosition)
    {
        this.gridSystem = gridSystem;
        this.gridPosition = gridPosition;
        coverTypes = new List<CoverType>();
        units = new List<Unit>();
        isValid = false;
    }

    public void AddUnit(Unit unit)
    {
        units.Add(unit);
    }

    public List<Unit> GetUnits()
    {
        return units;
    }


    //Return the unit on the grid
    public Unit GetUnit()
    {
        if (HasAnyUnit())
        {
            return units[0];
        }
        else
        {
            return null;
        }
    }

    public void RemoveUnit(Unit unit)
    {
        units.Remove(unit);
    }

    // Check is there any unit on the grid
    public bool HasAnyUnit()
    {
        return units.Count > 0;
    }

    //Return the Interact Object on the grid
    public IInteractable GetInteractableObject()
    {
        return interactableObject;
    }

    //Set this object a Interactable property
    public void SetInteractableObject(IInteractable interactable)
    {
        interactableObject = interactable;
        isValid = true;
    }

    public void RemoveCoverType(CoverType coverType)
    {
        coverTypes.Remove(coverType);
    }

    // Check if the grid is under cover
    public bool HasCover()
    {
        return coverTypes.Count > 0;
    }

    //Set this object's Cover Type
    public void SetCoverType(CoverType coverType)
    {
        coverTypes.Add(coverType);
    }

    //Return the cover Object on the grid
    public ICoverable GetCoverableObject()
    {
        return coverableObject;
    }

    //Set grid's coverage according to the surrounding object
    public void SetCoverableObject(ICoverable coverable)
    {
        coverableObject = coverable;
    }

    // Return if grid object is valid?
    public bool IsValid()
    {
        return isValid;
    }

    // Return if grid object is valid?
    public void ValidateGridObject(bool isValid)
    {
        this.isValid = isValid;
    }

    public override string ToString()
    {
        string unitString = "";
        foreach (Unit unit in units)
        {
            unitString += unit + "\n";
        }
        return gridPosition.ToString() + "\n" + unitString;
    }
}
