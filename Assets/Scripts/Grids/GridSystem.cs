﻿/*******************************************************************************************************
Class     : GridSystem
Author    : Justin Wang based on Hugo Cardoso(Code Monkey)
Date      : 10/12/2022-
Purpose   : Implement functions for grids aspect problems
Usage     : NULL
Memo      : Level Grid ⊆ Grid Systems ⊆ Grid Objects
            To place grid objects that has arrays of objects, meta data of this array and functions to interact with it.
            It started from (0,,0)
********************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GridSystem<TGridObject>
{
    private int width;
    private int height;
    private int floor;
    private float floorHeight;
    private float cellSize;
    private TGridObject[,] gridObjectArray;

    //What is func and generic?
    public GridSystem(int width, int height, int floor, float cellSize, float floorHeight, Func<GridSystem<TGridObject>, GridPosition, TGridObject>  createGridObject)
    {
        this.width = width;
        this.height = height;
        this.floor = floor;
        this.cellSize = cellSize;
        this.floorHeight = floorHeight;

        gridObjectArray = new TGridObject[width, height];

        for(int x = 0; x < width; x++)
        {
            for(int z = 0; z < height; z++)
            {
                GridPosition gridPosition = new GridPosition(x, z, floor);
                gridObjectArray[x, z] = createGridObject(this, gridPosition);
            }
        }
    }

    //Return world-position by getting grid-position
    public Vector3 GetWorldPosition(GridPosition gridPosition)
    {
        return
            new Vector3(gridPosition.x, 0, gridPosition.z) * cellSize + 
            new Vector3(0, gridPosition.floor, 0) * floorHeight;
    }
    
    //Return grid-position by getting world-position
    public GridPosition GetGridPosition(Vector3 worldPosition)
    {
        return new GridPosition(
            Mathf.RoundToInt(worldPosition.x / cellSize),
            Mathf.RoundToInt(worldPosition.z / cellSize),
            floor
            );
    }

    //Create debug grids object(the text on the floor)
    public void CreateDebugObjects(Transform debugPrefab)
    {
        if (GameObject.Find("GridDebugObjects"))
        {
            for (int x = 0; x < width; x++)
            {
                for (int z = 0; z < height; z++)
                {
                    GridPosition gridPosition = new GridPosition(x, z, floor);

                    Transform debugTransform = UnityEngine.Object.Instantiate(debugPrefab, GetWorldPosition(gridPosition), Quaternion.identity, GameObject.Find("GridDebugObjects").transform);
                    GridDebugObject gridDebugObject = debugTransform.GetComponent<GridDebugObject>();
                    gridDebugObject.SetGridObject(GetGridObject(gridPosition));
                }
            }
        }
        else
        {
            Debug.LogError("Container for Debug Objects is Not Found!!");
        }
    }

    //Get the particular grid by given world position which is pointed to this grid system
    public TGridObject GetGridObject(GridPosition gridPosition)
    {
        return gridObjectArray[gridPosition.x, gridPosition.z];
    }

    //Get the grid objects within this grid system 
    public TGridObject[,] GetGridObjects()
    {
        return gridObjectArray;
    }

    //Get cross adjacency grids
    public List<TGridObject> GetCrossGridObjects(GridPosition gridPosition, int range)
    {
        List<TGridObject> adjacencyGridObjects = new List<TGridObject>();
        int GridX = gridPosition.x;
        int GridZ = gridPosition.z;

        for (int i = GridZ - range; i <= GridZ + range; i++)
        {
            for (int j = GridX - range; j <= GridX + range; j++)
            {
                //Skip diagonal grids
                if (i != GridZ && j != GridX)
                {
                    continue;
                }
                else
                {
                    GridPosition grid = new GridPosition(j, i, floor);
                    if (IsValidGridPosition(grid))
                    {
                        adjacencyGridObjects.Add(GetGridObject(grid));
                    }
                }
            }
        }
        return adjacencyGridObjects;
    }

    //Get full adjacency grids
    public List<TGridObject> GetSquareGridObjects(GridPosition gridPosition, int range)
    {
        List<TGridObject> adjacencyGridObjects = new List<TGridObject>();
        int GridX = gridPosition.x;
        int GridZ = gridPosition.z;

        for (int i = GridZ - range; i <= GridZ + range; i++)
        {
            for (int j = GridX - range; j <= GridX + range; j++)
            {
                GridPosition grid = new GridPosition(j, i, floor);
                if (IsValidGridPosition(grid))
                {
                    adjacencyGridObjects.Add(GetGridObject(grid));
                }
            }
        }
        return adjacencyGridObjects;
    }

    //Check if grid's position are out of the grid system's boundary
    public bool IsValidGridPosition(GridPosition gridPosition)
    {
        return gridPosition.x >= 0 &&
               gridPosition.z >= 0 &&
               gridPosition.x < width &&
               gridPosition.z < height &&
               gridPosition.floor == floor;
    }

    public int GetWidth()
    {
        return width;
    }
    public int GetHeight()
    {
        return height;
    }
}
