﻿/*******************************************************************************************************
Class     : LevelGrid (SINGLETON)
Author    : Justin Wang based on Hugo Cardoso(Code Monkey)
Date      : 10/12/2022-
Purpose   : Function for interacting with grid system, get unit, add unit, remove unit, check unit,
            get grid-position, world-position, check grids and get the size of the grid system.
Usage     : OnAnyUnitMovedGridPosition(Event)
Memo      : It contains grid system which contains grid objects.
            Level Grid ⊆ Grid Systems ⊆ Grid Objects
            To control every units and objects in the game,
            making it into singleton which can be shared and accessed by every objects and systems.
********************************************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TestTools;

public class LevelGrid : MonoBehaviour
{
    // Singleton means everybody can access but only can create by itseelf and cannot have more than one
    public static LevelGrid Instance { get; private set; }

    public event EventHandler<OnAnyUnitMovedGridPositionEventArgs> OnAnyUnitMovedGridPosition;
    public class OnAnyUnitMovedGridPositionEventArgs : EventArgs
    {
        public Unit unit;
        public GridPosition fromGridPosition;
        public GridPosition toGridPosition;
    }

    [SerializeField] private bool DebugMode;
    [SerializeField] private Transform gridDebugObjectPrefab;

    [SerializeField] private int width;
    [SerializeField] private int height;
    [SerializeField] private float cellSize;
    [SerializeField] private int floorAmount;

    [SerializeField] private LayerMask obstacleLayerMask;
    [SerializeField] private LayerMask floorLayerMask;

    private List<GridSystem<GridObject>> gridSystems;

    void Awake()
    {
        if (Instance != null)
        {
            Debug.LogError("There's more than one LevelGrid!" + transform + " - " + Instance);
            Destroy(gameObject);
            return;
        }
        Instance = this;

        //Create Gridsystem
        gridSystems = new List<GridSystem<GridObject>>();
        for (int floor = 0; floor < floorAmount; floor++)
        {
            GridSystem<GridObject> gridSystem = new GridSystem<GridObject>(width, height, floor, cellSize, Properties.FLOOR_HEIGHT,
                (GridSystem<GridObject> g, GridPosition gridPosition) => new GridObject(g, gridPosition));

            gridSystems.Add(gridSystem);
        }

        Init();
    }

    private void Start()
    {
        PathFinding.Instance.Setup(width, height, cellSize, floorAmount);
    }

    private void Init()
    {
        //Each floor
        foreach (GridSystem<GridObject> gridSystem in gridSystems)
        {
            //Each grid objects
            foreach (GridObject gridObject in gridSystem.GetGridObjects())
            {
                Vector3 worldPosition = GetWorldPosition(gridObject.gridPosition);

                if (Physics.Raycast(
                    worldPosition + Vector3.up * Properties.raycastOffsetDistance,
                    Vector3.down,
                    Properties.FLOOR_HEIGHT - Properties.raycastOffsetDistance,
                    floorLayerMask))
                {
                    gridObject.ValidateGridObject(true);
                }

                if (Physics.Raycast(
                    worldPosition + Vector3.down * Properties.raycastOffsetDistance,
                    Vector3.up,
                    Properties.FLOOR_HEIGHT,
                    obstacleLayerMask))
                {
                    gridObject.ValidateGridObject(false);
                }
            }
            if (DebugMode)
                gridSystem.CreateDebugObjects(gridDebugObjectPrefab);
        }
    }

    //Get floor's gridSystem
    private GridSystem<GridObject> GetGridSystem(int floor)
    {
        return gridSystems[floor];
    }

    // With the given position, get the grid object from this level's grid system and add the unit property to it.
    public void AddUnitAtGridPosition(GridPosition gridPosition, Unit unit)
    {
        GridObject gridObject = GetGridSystem(gridPosition.floor).GetGridObject(gridPosition);
        gridObject.AddUnit(unit);
    }

    // With the given grid position, get unit on the grid object from this level's grid system.
    public Unit GetUnitAtGridPosition(GridPosition gridPosition)
    {
        GridObject gridObject = GetGridSystem(gridPosition.floor).GetGridObject(gridPosition);
        return gridObject.GetUnit();
    }

    // With the given position, get the grid object from this level's grid system and get the units property that it contains.
    public List<Unit> GetUnitsAtGridPosition(GridPosition gridPosition)
    {
        GridObject gridObject = GetGridSystem(gridPosition.floor).GetGridObject(gridPosition);
        return gridObject.GetUnits();
    }

    // With the given position, get the grid object from this level's grid system and remove the unit property on it.
    public void RemoveUnitAtGridPosition(GridPosition gridPosition, Unit unit)
    {
        GridObject gridObject = GetGridSystem(gridPosition.floor).GetGridObject(gridPosition);
        gridObject.RemoveUnit(unit);
    }

    // With the given position, remove the unit at the from grid and add the unit property at the to grid.
    public void UnitMovedGridPosition(Unit unit, GridPosition fromGridPosition, GridPosition toGridPosition)
    {
        RemoveUnitAtGridPosition(fromGridPosition, unit);
        AddUnitAtGridPosition(toGridPosition, unit);

        OnAnyUnitMovedGridPosition?.Invoke(this, new OnAnyUnitMovedGridPositionEventArgs
        {
            unit = unit,
            fromGridPosition = fromGridPosition,
            toGridPosition = toGridPosition,
        });

    }

    public int GetFloor(Vector3 worldPosition)
    {
        return Mathf.RoundToInt(worldPosition.y / Properties.FLOOR_HEIGHT);
    }

    // With the given real world position, get the grid object's grid position by calling its grid systems.
    public GridPosition GetGridPosition(Vector3 worldPosition)
    {
        int floor = GetFloor(worldPosition);
        return GetGridSystem(floor).GetGridPosition(worldPosition);
    }

    // With the given grid object's grid position, get the real world position by calling its grid systems.
    public Vector3 GetWorldPosition(GridPosition gridPosition) => GetGridSystem(gridPosition.floor).GetWorldPosition(gridPosition);

    // With the given grid position, check the grid object is inside the grid system by calling its grid systems.
    public bool IsValidGridPosition(GridPosition gridPosition)
    {
        bool isValid;

        if(gridPosition.floor < 0 || gridPosition.floor >= floorAmount)
        {
            isValid = false;
        }
        else if (GetGridSystem(gridPosition.floor).IsValidGridPosition(gridPosition))
        {
            isValid = GetGridSystem(gridPosition.floor).GetGridObject(gridPosition).IsValid();
        }
        else
        {
            isValid = false;
        }

        return isValid;
    }

    public int GetWidth() => GetGridSystem(0).GetWidth();

    public int GetHeight() => GetGridSystem(0).GetHeight();

    public int GetFloorAmount() => floorAmount;

    // With the given grid position, check if there has any unit on the grid object from this level's grid system.
    public bool HasAnyUnitOnGridPosition(GridPosition gridPosition)
    {
        GridObject gridObject = GetGridSystem(gridPosition.floor).GetGridObject(gridPosition);
        return gridObject.HasAnyUnit();
    }

    // With the given position, get the grid object from this level's grid system and get the Interactable object.
    public IInteractable GetInteractableAtGridPosition(GridPosition gridPosition)
    {
        GridObject gridObject = GetGridSystem(gridPosition.floor).GetGridObject(gridPosition);
        return gridObject.GetInteractableObject();
    }

    // With the given position, get the grid object from this level's grid system and set the Interactable property.
    public void SetInteractableAtGridPosition(GridPosition gridPosition, IInteractable interactable)
    {
        GridObject gridObject = GetGridSystem(gridPosition.floor).GetGridObject(gridPosition);
        gridObject.SetInteractableObject(interactable);

    }

    // With the given position, get the grid object from this level's grid system and get the Coverable Object.
    public ICoverable GetCoverableAtGridPosition(GridPosition gridPosition)
    {
        GridObject gridObject = GetGridSystem(gridPosition.floor).GetGridObject(gridPosition);
        return gridObject.GetCoverableObject();
    }

    public bool beingCover(GridPosition gridPosition)
    {
        GridObject gridObject = GetGridSystem(gridPosition.floor).GetGridObject(gridPosition);
        return gridObject.HasCover();
    }

    // With the given position, get the grid object from this level's grid system and set the Coverable Object.
    public void SetCoverableAtGridPosition(GridPosition gridPosition, ICoverable coverable)
    {
        GridObject gridObject = GetGridSystem(gridPosition.floor).GetGridObject(gridPosition);
        gridObject.SetCoverableObject(coverable);

        foreach (GridObject adjacencyGridObject in GetAdjacencyGridObjects(gridPosition))
        {
            adjacencyGridObject.SetCoverType(coverable.cover);

            if (adjacencyGridObject.GetUnit())
                adjacencyGridObject.GetUnit().SetUnitUnderCover(true);
        }
    }

    // With the given position, get the grid object from this level's grid system and remove the Coverable Object.
    public void RemoveCoverableAtGridPosition(GridPosition gridPosition)
    {
        GridObject gridObject = GetGridSystem(gridPosition.floor).GetGridObject(gridPosition);
        CoverType cover = gridObject.GetCoverableObject().cover;
        // Clear cover
        gridObject.SetCoverableObject(null);

        foreach (GridObject adjacencyGridObject in GetAdjacencyGridObjects(gridPosition))
        {
            Unit unit = adjacencyGridObject.GetUnit();
            adjacencyGridObject.RemoveCoverType(cover);
            
            if (unit && !adjacencyGridObject.HasCover())
            {
                unit.SetUnitUnderCover(false);
            }
        }
    }

    //Get adjacency grids
    public List<GridObject> GetAdjacencyGridObjects(GridPosition gridPosition) => GetGridSystem(gridPosition.floor).GetCrossGridObjects(gridPosition, 1);

    //Get full square grids
    public List<GridObject> GetSquareGridObjects(GridPosition gridPosition, int range) => GetGridSystem(gridPosition.floor).GetSquareGridObjects(gridPosition, range);
    public List<GridObject> GetSquareGridObjects(Vector3 destination, int range) => GetSquareGridObjects(GetGridPosition(destination), range);
}
