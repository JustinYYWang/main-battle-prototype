/*******************************************************************************************************
Struct    : GridSystemVisualController
Author    : Justin Wang based on Hugo Cardoso(Code Monkey)
Date      : 10/12/2022-
Purpose   : Controller for show or closed the grid visualized
Usage     : NULL
Memo      : NULL
********************************************************************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridSystemVisualController : MonoBehaviour
{
    [SerializeField] private MeshRenderer meshRenderer;

    public void Show(Material material)
    {
        meshRenderer.enabled = true;
        meshRenderer.material = material;
    }

    public void Hide()
    {
        meshRenderer.enabled = false;
    }
}
