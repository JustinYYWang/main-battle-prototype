/*******************************************************************************************************
Struct    : GridDebugObject
Author    : Justin Wang based on Hugo Cardoso(Code Monkey)
Date      : 10/12/2022-
Purpose   : It defined Grid Debug Object, showing grid-position and grid information on the floor
Usage     : NULL
Memo      : NULL
********************************************************************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GridDebugObject : MonoBehaviour
{
    [SerializeField] private TextMeshPro textMeshPro;
    private object DebugObject;

    public virtual void SetGridObject(object DebugObject)
    {
        this.DebugObject = DebugObject;
    }

    protected virtual void Update()
    {
        if (DebugObject is GridObject gridObject)
        {
            if (gridObject.IsValid())
                textMeshPro.text = DebugObject.ToString();
            else
            {
                textMeshPro.text = null;
            }
        }
        else
        {
            textMeshPro.text = DebugObject.ToString();
        }
    }
}
