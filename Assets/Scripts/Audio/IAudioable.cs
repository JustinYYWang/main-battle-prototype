public interface IAudioable
{
    void AudioSourceInit();

    void Play(string name, bool loop = false);

    void Stop(string name, bool stopImediate = false);
}
