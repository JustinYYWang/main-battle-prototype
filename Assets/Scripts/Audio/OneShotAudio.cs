using UnityEngine;

public class OneShotAudio : MonoBehaviour
{
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private string soundName;
 
    void Awake()
    {
        audioSource.clip = AudioManager.Instance.Get3DOneShotClip(soundName);
        audioSource.Play();
    }
}
