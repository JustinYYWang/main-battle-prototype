using UnityEngine;
using System;
using System.Collections.Generic;


//Have loops, have packs,  General Objects, 2D sound
public class AudioManager : MonoBehaviour
{
    public List<Sound> sounds;
    public List<Sound> oneShotSounds;
    private Dictionary<string, List<Sound>> playList;
    private string lastPlayName;

    public static AudioManager Instance;

    // Initillize every sound source in these scene
    void Awake()
    {
        if (Instance != null)
        {
            Debug.LogError("There's more than one AudioManager!" + transform + " - " + Instance);
            Destroy(gameObject);
            return;
        }
        Instance = this;

        //We don't want sound being interupted during scenes.
        DontDestroyOnLoad(this);

        foreach (Sound sound in sounds)
        {
            sound.source = gameObject.AddComponent<AudioSource>();
            sound.source.clip = sound.clip;
            sound.source.volume = sound.volume;
            sound.source.pitch = sound.pitch;
            sound.source.loop = sound.loop;
        }

        playList = new Dictionary<string, List<Sound>>();
    }

    public void Play(string name, bool loop = false)
    {
        //Sort every query for playlist, shuffle the value
        if (!playList.ContainsKey(name))
        {
            List<Sound> soundPack = sounds.FindAll(sound => sound.name == name);
            if (soundPack == null)
            {
                Debug.LogWarning("Sound: " + name + " not found!");
                return;
            }

            if (soundPack.Count == 1)
            {
                soundPack[0].source.loop = loop;
                soundPack[0].source.Play();
            }
            else
            {
                List<Sound> sortedSoundPack = GameModifier.Shuffle(soundPack);
                sortedSoundPack[0].source.Play();
                lastPlayName = sortedSoundPack[0].name;
                sortedSoundPack.RemoveAt(0);
                playList.Add(name, sortedSoundPack);
            }
        }
        else
        {
            playList[name][0].source.Play();
            lastPlayName = playList[name][0].name;
            playList[name].RemoveAt(0);
            if (playList[name].Count == 0)
                playList.Remove(name);
        }
    }

    public AudioClip Get3DOneShotClip(string name)
    {
        AudioClip clip = null;
        if (!playList.ContainsKey(name))
        {
            List<Sound> soundPack = oneShotSounds.FindAll(sound => sound.name == name);
            if (soundPack == null)
            {
                Debug.LogWarning("Sound: " + name + " not found!");
                return clip;
            }

            if (soundPack.Count == 1)
            {
                return soundPack[0].clip;
            }
            else
            {
                List<Sound> sortedSoundPack = GameModifier.Shuffle(soundPack);
                lastPlayName = sortedSoundPack[0].name;
                clip = sortedSoundPack[0].clip;
                sortedSoundPack.RemoveAt(0);
                playList.Add(name, sortedSoundPack);
                return clip;
            }
        }
        else
        {
            clip = playList[name][0].clip;
            lastPlayName = playList[name][0].name;
            playList[name].RemoveAt(0);
            if (playList[name].Count == 0)
                playList.Remove(name);
            return clip;
        }
    }

    public void Stop(string name, bool stopImediate = false)
    {
        List<Sound> soundPack = sounds.FindAll(sound => sound.name == name);
        if (soundPack == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }

        if (soundPack.Count == 1)
        {
            //Wait until loop finished
            if (!stopImediate)
                soundPack[0].source.loop = false;
            else
                soundPack[0].source.Stop();
        }
        else
        {
            //Wait until loop finished
            AudioSource targetSource = soundPack.Find(sound => sound.name == lastPlayName).source;
            if (!stopImediate)
                targetSource.loop = false;
            else
                targetSource.Stop();
        }
    }
}
