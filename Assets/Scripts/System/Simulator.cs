using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Simulator : MonoBehaviour
{
    public static Simulator Instance { get; private set; }

    public Scene _simulationScene;
    public PhysicsScene _physicsScene;
    [SerializeField] private Transform _obstaclesParent;


    void Start()
    {
        DestructableObject.OnAnyDestroyed += Destructable_OnAnyDestroyed;
        CreatePhysicScene();
    }

    private void Awake()
    {
        if (Instance != null)
        {
            Debug.LogError("There's more than one Simulator!" + transform + " - " + Instance);
            Destroy(gameObject);
            return;
        }
        Instance = this;
    }

    private void CreatePhysicScene()
    {
        _simulationScene = SceneManager.CreateScene("Simulation", new CreateSceneParameters(LocalPhysicsMode.Physics3D));
        _physicsScene = _simulationScene.GetPhysicsScene();

        foreach (Transform obj in _obstaclesParent)
        {
            //Change it to recursive
            var ghostObj = Instantiate(obj.gameObject, obj.position, obj.rotation); 
            if (ghostObj.transform.childCount > 0)
            {
                foreach (Transform childObj in ghostObj.transform)
                {
                    if (childObj.GetComponent<Renderer>())
                        childObj.GetComponent<Renderer>().enabled = false;
                    else if (childObj.GetComponentInChildren<Renderer>())
                        childObj.GetComponentInChildren<Renderer>().enabled = false;
                }
            }
            else
            {
                if (ghostObj.GetComponent<Renderer>())
                    ghostObj.GetComponent<Renderer>().enabled = false;
                else if (ghostObj.GetComponentInChildren<Renderer>())
                    ghostObj.GetComponentInChildren<Renderer>().enabled = false;
            }

            SceneManager.MoveGameObjectToScene(ghostObj, _simulationScene);
        }
    }

    private void Destructable_OnAnyDestroyed(object sender, EventArgs e)
    {

    }
}
