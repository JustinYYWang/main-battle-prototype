using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseWorld : MonoBehaviour
{
    private static MouseWorld instance;
    [SerializeField] private LayerMask mousePlaneLayerMask;

    private void Awake()
    {
        instance = this;
    }

    // Update is called once per frame
    private void Update()
    {
        transform.position = GetPosition();
    }

    //Return the first mouse plane it hits
    public static Vector3 GetPosition()
    {
        Ray ray = Camera.main.ScreenPointToRay(InputManager.Instance.GetMouseScreenPosition());
        Physics.Raycast(ray, out RaycastHit raycastHit, float.MaxValue, instance.mousePlaneLayerMask);
        return raycastHit.point;
    }

    //Return the mouse plane that has mesh visible
    public static Vector3 GetPositionOnlyHitVisible()
    {
        Ray ray = Camera.main.ScreenPointToRay(InputManager.Instance.GetMouseScreenPosition());
        RaycastHit[] raycastHits = Physics.RaycastAll(ray, float.MaxValue, instance.mousePlaneLayerMask);
        //Sort by its distance to know which is closer
        System.Array.Sort(raycastHits, (RaycastHit raycastHitA, RaycastHit raycastHitB) =>
        {
            return Mathf.RoundToInt(raycastHitA.distance - raycastHitB.distance);
        });

        foreach (RaycastHit hit in raycastHits)
        {
            if (hit.transform.TryGetComponent(out Renderer renderer))
            {
                if (renderer.enabled)
                {
                    return hit.point;
                }
            }
        }
        return Vector3.zero;
    }

}
