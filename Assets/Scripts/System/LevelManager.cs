using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Unity.VisualScripting;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    [SerializeField] private GameObject hider1;
    [SerializeField] private GameObject hider2;
    [SerializeField] private GameObject hider3;
    [SerializeField] private GameObject hider4;
    [SerializeField] private GameObject hider5;
    [SerializeField] private GameObject hider6;
    [SerializeField] private GameObject hider7;
    [SerializeField] private List<GameObject> enemySquad1;
    [SerializeField] private List<GameObject> enemySquad2;
    [SerializeField] private List<GameObject> enemySquad3;
    [SerializeField] private List<GameObject> enemySquad4;
    [SerializeField] private List<GameObject> enemySquad5;
    [SerializeField] private List<GameObject> enemySquad6;
    [SerializeField] private Door door1;
    [SerializeField] private Door door2;
    [SerializeField] private Door door3;
    [SerializeField] private Door door4;
    [SerializeField] private Door door5;
    [SerializeField] private Door door6;

    private bool door6IsOpened;

    private void Start()
    {
        LevelGrid.Instance.OnAnyUnitMovedGridPosition += LevelGrid_OnAnyUnitMovedGridPosition;
        door1.OnDoorOpened += (object sender, EventArgs e) =>
        {
            SetGameObjectActive(hider1, false);
        };
        door2.OnDoorOpened += (object sender, EventArgs e) =>
        {
            SetGameObjectActive(hider2, false);
            SetGameObjectActive(enemySquad1, true);
        };
        door3.OnDoorOpened += (object sender, EventArgs e) =>
        {
            SetGameObjectActive(hider3, false);
            SetGameObjectActive(enemySquad2, true);
        };
        door4.OnDoorOpened += (object sender, EventArgs e) =>
        {
            SetGameObjectActive(hider4, false);
            SetGameObjectActive(enemySquad3, true);
        };
        door5.OnDoorOpened += (object sender, EventArgs e) =>
        {
            SetGameObjectActive(hider6, false);
            SetGameObjectActive(enemySquad5, true);
        };
        door6.OnDoorOpened += (object sender, EventArgs e) =>
        {
            door6IsOpened = true;
        };

        AudioManager.Instance.Play("Scene_BGM", true);
    }

    private void LevelGrid_OnAnyUnitMovedGridPosition(object sender, LevelGrid.OnAnyUnitMovedGridPositionEventArgs e)
    {
        if (Enumerable.Range(1, 8).Contains(e.toGridPosition.x) &&  e.toGridPosition.z == 20)
        {
            SetGameObjectActive(hider5, false);
            SetGameObjectActive(enemySquad4, true);
        }

        if (Enumerable.Range(21, 2).Contains(e.toGridPosition.x) && e.toGridPosition.z == 30 && door6IsOpened)
        {
            SetGameObjectActive(hider7, false);
            SetGameObjectActive(enemySquad6, true);
        }
    }

    private void SetGameObjectActive(List<GameObject> gameObjects, bool isActive)
    {
        foreach (GameObject gameObject in gameObjects)
        {
            gameObject.SetActive(isActive);
        }
    }

    private void SetGameObjectActive(GameObject gameObject, bool isActive)
    {
        gameObject.SetActive(isActive);
    }

}
