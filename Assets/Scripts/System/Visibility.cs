using System.Linq;
using UnityEngine;

public class Visibility : MonoBehaviour
{
    private Renderer[] renderers;
    [SerializeField] private Renderer[] rendererIgnoreList;

    private int floor;

    [SerializeField] float object_CameraOffset = 3.8f;
    [SerializeField] private bool isDynamicObject;

    private void Awake()
    {
        renderers = GetComponentsInChildren<Renderer>(true);
    }

    private void Start()
    {
        floor = LevelGrid.Instance.GetFloor(transform.position);

        if (floor == 0 && !isDynamicObject)
            Destroy(this);
    }

    private void Update()
    {
        //Fetch its floor status continuously
        //TODO: Fetch floor status by event
        if (isDynamicObject)
            floor = LevelGrid.Instance.GetFloor(transform.position);

        //Calculate the object height and compare with camera
        float cameraHeight = CameraController.instance.GetCameraHeight();

        bool showObject = cameraHeight > Properties.FLOOR_HEIGHT * floor + object_CameraOffset;

        if (showObject || floor ==0)
            Show();
        else
            Hide();
    }

    private void Show()
    {
        foreach (Renderer renderer in renderers)
        {
            if (rendererIgnoreList.Contains(renderer)) continue;
            renderer.enabled = true;
        }
    }

    private void Hide()
    {
        foreach (Renderer renderer in renderers)
        {
            if (rendererIgnoreList.Contains(renderer)) continue;
            renderer.enabled = false;
        }
    }
}
