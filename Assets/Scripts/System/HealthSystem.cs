using System;
using System.Collections;
using System.Collections.Generic;
using Tatooine;
using UnityEngine;

public class HealthSystem : MonoBehaviour
{
    public event EventHandler OnDead;
    public event EventHandler OnDamaged;

    [SerializeField] private float health;

    private UnitStatus unitController;

    private void Awake()
    {
        unitController = GetComponent<UnitStatus>();
    }

    private void Start()
    {
        health = unitController.MaxHealth;
    }

    //Damage the unit by deduct health point, if is less than 0. It died.
    public void Damage(float damageAmount)
    {
        health -= damageAmount;

        if(health < 0) health = 0;

        OnDamaged?.Invoke(this, EventArgs.Empty);

        if (health == 0) Die();

        Debug.Log(gameObject.name + " health: " + health);
    }

    //Unit dead logical and animation implement
    private void Die()
    {
        OnDead?.Invoke(this, EventArgs.Empty);
    }

    public float GetHealthNormalized()
    {
        return (float)health / unitController.MaxHealth;
    }
}
