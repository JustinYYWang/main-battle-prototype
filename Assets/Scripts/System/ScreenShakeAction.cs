using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This scripts listen to the event and shake the screen when the event has invoked
public class ScreenShakeAction : MonoBehaviour
{
    void Start()
    {
        ShootAction.OnAnyShoot += ShootAction_OnAnyShoot;
        GrenadeProjectile.OnAnyGrenadeExploded += GrenadeProjectile_OnAnyGrenadeExploded;
        MeleeAction.OnAnyMeleeAttackHit += MeleeAction_OnAnyMeleeAttackHit;
    }

    private void MeleeAction_OnAnyMeleeAttackHit(object sender, EventArgs e)
    {
        ScreenShaker.Instance.Shake(2f);
    }

    private void ShootAction_OnAnyShoot(object sender, ShootAction.ShootingArgs e)
    {
        ScreenShaker.Instance.Shake();
    }


    private void GrenadeProjectile_OnAnyGrenadeExploded(object sender, EventArgs e)
    {
        ScreenShaker.Instance.Shake(5f);
    }
}
