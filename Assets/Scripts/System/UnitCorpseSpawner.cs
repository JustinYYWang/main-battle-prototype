using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitCorpseSpawner : MonoBehaviour
{
    [SerializeField] private Transform ragdollPrefab;
    [SerializeField] private Transform originalRootBone;

    private HealthSystem healthSystem;

    private void Awake()
    {
        healthSystem = GetComponent<HealthSystem>();

        healthSystem.OnDead += HealthSystem_OnDead;
    }

    //Handle event OnDead from HealthSystem script, spawn corpse when unit is dead
    private void HealthSystem_OnDead(object sender, EventArgs e)
    {
        Transform ragdollTransform = Instantiate(ragdollPrefab, transform.position, transform.rotation);
        UnitCorpse unitCorpse = ragdollTransform.GetComponent<UnitCorpse>();
        unitCorpse.Setup(originalRootBone);
    }
}
