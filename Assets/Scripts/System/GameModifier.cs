using System.Collections.Generic;
using UnityEngine;

public class GameModifier : MonoBehaviour
{
    //Shuufle every elements one-by-one in the list
    public static List<T> Shuffle<T>(List<T> list)
    {
        int current = list.Count;

        while (current > 1)
        {
            current--;
            int k = Random.Range(0, current);
            T value = list[k];
            list[k] = list[current];
            list[current] = value;
        }

        return list;
    }

    //Handle damage point
    public static float damageModifier(float baseAmount, ShootAction.ShootingArgs shootingArgs)
    {
        float modifiedAmount = 0;

        //Height Modifier(Required proper math)
        float heightDiff = shootingArgs.shootingUnit.GetGridPosition().floor - shootingArgs.targetUnit.GetGridPosition().floor;
        modifiedAmount = baseAmount * ((heightDiff + 2) / 2);
        print("Height Difference: " + heightDiff);

        //Cover type modifier
        switch (shootingArgs.targetCoverType)
        {
            case CoverType.None:
                break;
            case CoverType.One_Quarter: 
                modifiedAmount = (modifiedAmount / 4) * 3; 
                break;
            case CoverType.Half:
                modifiedAmount = modifiedAmount / 2;
                break;
            case CoverType.Three_Quarter:
                modifiedAmount = modifiedAmount / 4;
                break;
            case CoverType.Full:
                modifiedAmount = 1;
                break;
        }
        print("Target Cover " + shootingArgs.targetCoverType);

        return modifiedAmount;
    }
}
