/******************************************************************************
                            Control camera with input
                            WASD for moving camera
                            QE for rotating camera
                            Scroll for zooming camera
******************************************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraController : MonoBehaviour
{
    public static CameraController instance { get; private set; }

    float moveSpeed = 10f;
    float rotationSpeed = 100f;
    float zoomSpeed = 5f;
    float zoomIncreaseAmount = 1f;

    [SerializeField] private CinemachineVirtualCamera cinemachineVirtualCamera;

    private CinemachineTransposer cinemachineTransposer;
    private Vector3 targetFollowOffset;

    private void Awake()
    {
        instance = this;
    }

    //Get cinemachine transposer for handle camera
    private void Start()
    {
        cinemachineTransposer = cinemachineVirtualCamera.GetCinemachineComponent<CinemachineTransposer>();
        targetFollowOffset = cinemachineTransposer.m_FollowOffset;
    }

    // Update is called once per frame
    void Update()
    {
        HandleMovement();
        HandleRotation();
        HandleZoom();
    }

    // Move camera
    private void HandleMovement()
    {
        Vector2 inputMoveDir = InputManager.Instance.GetCameraMoveVector();

        //Get the direction with the amount of input, we want camera move relatively.
        Vector3 moveVector = transform.forward * inputMoveDir.y + transform.right * inputMoveDir.x * 1.2f;
        transform.position += moveVector * moveSpeed * Time.deltaTime;
    }

    // Rotate camera
    private void HandleRotation()
    {
        Vector3 rotationVector = new Vector3(0, 0, 0);

        rotationVector.y = InputManager.Instance.GetCameraRotateAmount();

        //Just rotate the camera with the amount of input
        transform.eulerAngles += rotationVector * rotationSpeed * Time.deltaTime;
    }

    // Zoom camera
    private const float MIN_FOLLOW_Y_OFFSET = 2f;
    private const float MAX_FOLLOW_Y_OFFSET = 15f;
    private void HandleZoom()
    {
        targetFollowOffset.y += InputManager.Instance.GetCameraZoomAmount() * zoomIncreaseAmount;

        // Set the boundary for zooming camera, then use the cinemachine's offset variable to zoom camera smoothly
        targetFollowOffset.y = Mathf.Clamp(targetFollowOffset.y, MIN_FOLLOW_Y_OFFSET, MAX_FOLLOW_Y_OFFSET);
        cinemachineTransposer.m_FollowOffset =
            Vector3.Lerp(cinemachineTransposer.m_FollowOffset, targetFollowOffset, Time.deltaTime * zoomSpeed);
    }

    public float GetCameraHeight()
    {
        return targetFollowOffset.y;
    }
}