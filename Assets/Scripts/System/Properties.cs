using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

public static class Properties
{
    public const int MousePlanMask = 6;
    public const float FLOOR_HEIGHT = 3f;
    public const float raycastOffsetDistance = 1f;
    public const float unitShoulderHeight = 1.7f;
    public const float reachedTargetDistance = .2f;

    public static T CopyComponent<T>(T original, GameObject destination) where T : Component
    {
        System.Type type = original.GetType();
        Component copy = destination.AddComponent(type);
        System.Reflection.FieldInfo[] fields = type.GetFields();
        foreach (System.Reflection.FieldInfo field in fields)
        {
            field.SetValue(copy, field.GetValue(original));
        }
        return copy as T;
    }
}
