using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class UnitAnimator : MonoBehaviour, IAudioable
{
    [SerializeField] private Animator animator;
    [SerializeField] private Transform bulletProjectilePrefab;
    [SerializeField] private Transform shootPointTransform;
    [SerializeField] private Transform rifleTransform;
    [SerializeField] private Transform rifleBackHandTransform;

    [SerializeField] private List<Sound> sounds;
    [SerializeField] private AudioSource audioSource;
    private Dictionary<string, List<Sound>> playList;

    //Check and get object's component and handle the animation.
    private void Awake()
    {
        if (TryGetComponent(out MoveAction moveAction))
        {
            moveAction.OnStartMoving += MoveAction_OnStartMoving;
            moveAction.OnStopMoving += MoveAction_OnStopMoving;
            moveAction.OnChangedFloor += MoveAction_OnChangedFloor;
        }

        if (TryGetComponent(out ShootAction shootAction))
        {
            shootAction.OnShoot += ShootAction_OnShoot;
        }

        if (TryGetComponent(out MeleeAction meleeAction))
        {
            meleeAction.OnMeleeActionStarted += MeleeAction_OnMeleeActionStarted;
            meleeAction.OnMeleeActionCompleted += MeleeAction_OnMeleeActionCompleted;
        }

        if (TryGetComponent(out Unit unit))
            unit.OnChangeCoverStatus += OnCoverStatusChanged;

        AudioSourceInit();
    }

    private void Start()
    {
        HoldRifle();
    }

    private void OnCoverStatusChanged(object sender, EventArgs e)
    {
        animator.SetBool("IsCovering", !animator.GetBool("IsCovering"));
    }

    //Handle event OnMeleeActionStarted, trigger melee attack animator
    private void MeleeAction_OnMeleeActionStarted(object sender, EventArgs e)
    {
        MeleeMode();
        animator.SetTrigger("Melee");
    }

    //Handle event OnMeleeActionCompleted, trigger melee attack animator
    private void MeleeAction_OnMeleeActionCompleted(object sender, EventArgs e)
    {
        HoldRifle();
    }

    //Handle event OnShoot, trigger shooting animator and create bullet vfx
    private void ShootAction_OnShoot(object sender, ShootAction.ShootingArgs e)
    {
        animator.SetTrigger("Shoot");

        Transform bulletProjectileTransform =
            Instantiate(bulletProjectilePrefab, shootPointTransform.position, Quaternion.identity);
        BulletProjectileVFX bulletProjectileVFX = bulletProjectileTransform.GetComponent<BulletProjectileVFX>();

        Play("Fire");

        Vector3 targetUnitShootAtPosition = e.targetUnit.GetWorldPosition();

        float unitShoulderHeight = 1.7f; // <- be caution for this way of approching
        targetUnitShootAtPosition.y += unitShoulderHeight;
        bulletProjectileVFX.Setup(targetUnitShootAtPosition);
    }

    //Handle event OnStartMoving, activate moving animator
    private void MoveAction_OnStartMoving(object sender, EventArgs e)
    {
        animator.SetBool("IsWalking", true);
        Play("Walk", true);
    }

    //Handle event OnStartMoving, deactivate moving animator
    private void MoveAction_OnStopMoving(object sender, EventArgs e)
    {
        animator.SetBool("IsWalking", false);
        Stop("Walk");
    }

    //Handle event OnChangedFloorStarted, activate jumping animator
    private void MoveAction_OnChangedFloor(object sender, MoveAction.OnChangedFloorArgs e)
    {
        if (e.targetGridPosition.floor > e.unitGridPosition.floor)
        {
            // Jump
            animator.SetTrigger("JumpUp");
        }
        else
        {
            // Drop
            animator.SetTrigger("JumpDown");
        }
    }

    private void HoldRifle()
    {
        rifleTransform.gameObject.SetActive(true);
        rifleBackHandTransform.gameObject.SetActive(false);
    }

    private void MeleeMode()
    {
        rifleTransform.gameObject.SetActive(false);
        rifleBackHandTransform.gameObject.SetActive(true);
    }


    //
    //
    //Audio Interface
    //Have loops, have packs, per object, 3D sound
    public void AudioSourceInit()
    {
        playList = new Dictionary<string, List<Sound>>();
    }


    public void Play(string name, bool loop = false)
    {
        List<Sound> soundPack = sounds.FindAll(sound => sound.name == name);
        if (soundPack == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }

        if (soundPack.Count == 1)
        {
            PlayAudio(soundPack[0]);
        }
        else
        {
            if (playList.ContainsKey(name))
            {
                PlayAudio(playList[name][0]);
                playList[name].RemoveAt(0);
            }
            else
            {
                List<Sound> sortedSoundPack = GameModifier.Shuffle(soundPack);
                PlayAudio(sortedSoundPack[0]);
                sortedSoundPack.RemoveAt(0);

                playList.Add(name, sortedSoundPack);
            }
        }
    }

    private void PlayAudio(Sound sound)
    {
        if (audioSource.isPlaying)
        {
            if (sound.loop)
            {
                AudioSource audioSourceClone = gameObject.AddComponent<AudioSource>();
                audioSourceClone.loop = sound.loop;
                audioSourceClone.volume = sound.volume;
                audioSourceClone.pitch = sound.pitch;
                audioSourceClone.clip = sound.clip;
                audioSourceClone.dopplerLevel = audioSource.dopplerLevel;
                audioSourceClone.spatialBlend = audioSource.spatialBlend;
                audioSourceClone.maxDistance = audioSource.maxDistance;
                audioSourceClone.rolloffMode= audioSource.rolloffMode;
                audioSourceClone.Play();
            }
            else
                audioSource.PlayOneShot(sound.clip, sound.volume);
        }
        else
        {
            audioSource.loop = sound.loop;
            audioSource.volume = sound.volume;
            audioSource.pitch = sound.pitch;
            audioSource.clip = sound.clip;
            audioSource.Play();
        }
    }

    public void Stop(string name, bool stopImediate = false)
    {
        List<Sound> soundPack = sounds.FindAll(sound => sound.name == name);
        if (soundPack == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }

        if (soundPack.Count == 1)
        {
            AudioSource[] childSources = gameObject.GetComponents<AudioSource>();
            if (childSources.Length > 1)
            {
                foreach (AudioSource source in childSources)
                {
                    if(source.clip == soundPack[0].clip)
                    {
                        if (!stopImediate)
                            source.loop = false;
                        else
                            source.Stop();
                    }

                    if (audioSource == source)
                        continue;

                    if (!source.isPlaying)
                        Destroy(source);
                }
            }
            else
            {
                if (!stopImediate)
                    audioSource.loop = false;
                else
                    audioSource.Stop();
            }
        }
        else
        {
            Debug.LogWarning("Sound: " + name + " setting is wrong!");
            return;
        }
    }
}
