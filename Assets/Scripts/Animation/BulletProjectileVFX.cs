using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletProjectileVFX : MonoBehaviour
{
    [SerializeField] private TrailRenderer trailRenderer;
    [SerializeField] private Transform bulletHitVFXPrefab;

    private Vector3 targetPosition;

    //Initial object, getting target position
    public void Setup(Vector3 targetPosition)
    {
        this.targetPosition = targetPosition;
    }

    //Forward the bullet with projectile vfx toward target
    float moveSpeed = 200f;
    private void Update()
    {
        Vector3 moveDir = (targetPosition - transform.position).normalized;

        //Remain distance between target and the bullet 
        float distanceBeforeMoving = Vector3.Distance(transform.position, targetPosition);

        //Move some distance
        transform.position += moveDir * moveSpeed * Time.deltaTime;

        //After moving some distance, see the distance between target and the bullet
        float distanceAfterMoving = Vector3.Distance(transform.position, targetPosition);

        //Bullet has by passed/reached the target.
        if (distanceBeforeMoving < distanceAfterMoving)
        {
            //Don't render vfx if it has already passed the target
            transform.position = targetPosition;

            //Keep the trail until it die natraually
            trailRenderer.transform.parent = null;
            Destroy(gameObject);

            Instantiate(bulletHitVFXPrefab, targetPosition, Quaternion.identity);
        }
    }
}
