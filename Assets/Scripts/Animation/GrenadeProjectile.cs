using System;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeProjectile : MonoBehaviour
{
    public static event EventHandler OnAnyGrenadeExploded;

    [SerializeField] private Transform grenadeExplodeVFXPrefab;
    [SerializeField] private TrailRenderer trailRenderer;

    public float delay = 3f;
    private float countDown;
    public float firingAngle;

    private Action onGrenadeBehaviourComplete;

    private void Update()
    {
        countDown -= Time.deltaTime;
        if (countDown <= 0f)
        {
            int damageRadius = 2;

            //Doing damage to the targets in the area
            //Maybe bring back OverlapSphere?
            List<GridObject> explotionGrids = LevelGrid.Instance.GetSquareGridObjects(transform.position, damageRadius);
            
            foreach (GridObject grid in explotionGrids)
            {
                Debug.Log(grid.gridPosition);
                RaycastHit[] hits = Physics.RaycastAll(
                        LevelGrid.Instance.GetWorldPosition(grid.gridPosition) + Vector3.down * Properties.raycastOffsetDistance,
                        Vector3.up,
                        Properties.FLOOR_HEIGHT);

                foreach (RaycastHit hit in hits)
                {
                    if (hit.collider.TryGetComponent(out IHitable hitable))
                    {
                        hitable.Damage(30);
                    }
                }
            }

            /*Collider[] colliders = Physics.OverlapSphere(targetPosition, damageRadius);
            
            //Doing damage to the targets in the area
            foreach (Collider collider in colliders)
            {
                if (collider.TryGetComponent(out IHitable hitable)){
                    hitable.Damage(30);

                }
            }*/

            OnAnyGrenadeExploded?.Invoke(this, EventArgs.Empty);

            //trailRenderer.transform.parent = null;
            Instantiate(grenadeExplodeVFXPrefab, transform.position + Vector3.up, Quaternion.identity);
            Destroy(gameObject);

            onGrenadeBehaviourComplete();
        }
    }

    //Set the complete action, detination and animation
    public void Setup(Action onGrenadeBehaviourComplete)
    {
        countDown = delay;
        this.onGrenadeBehaviourComplete = onGrenadeBehaviourComplete;
    }
}
