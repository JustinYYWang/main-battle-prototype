using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFinding : MonoBehaviour
{
    public static PathFinding Instance { get; private set; }


    private const int MOVE_STRAIGHT_COST = 10;
    private const int MOVE_DIAGONAL_COST = 14;

    [SerializeField] private Transform gridDebugObjectPrefab;
    [SerializeField] private LayerMask obstacleLayerMask;
    [SerializeField] private LayerMask floorLayerMask;
    [SerializeField] private Transform PathFindingLinkContainer;

    private int width;
    private int height;
    private int floorAmount;
    private float cellSize;
    private List<GridSystem<PathNode>> gridSystemList;
    private List<PathFindingLink> pathFindingLinkList;

    private void Awake()
    {
        if (Instance != null)
        {
            Debug.LogError("There's more than one PathFinding!" + transform + " - " + Instance);
            Destroy(gameObject);
            return;
        }
        Instance = this;
    }

    //Set up the logic layer of the grid system, especially for path relative system
    public void Setup(int width, int height, float cellSize, int floorAmount)
    {
        this.width = width;
        this.height = height;
        this.floorAmount = floorAmount;
        this.cellSize = cellSize;

        gridSystemList = new List<GridSystem<PathNode>>();
        for(int floor = 0; floor < floorAmount; floor++)
        {
            GridSystem<PathNode> gridSystem = new GridSystem<PathNode>(width, height, floor, cellSize, Properties.FLOOR_HEIGHT,
                (GridSystem<PathNode> g, GridPosition gridPosition) => new PathNode(gridPosition));

            //gridSystem.CreateDebugObjects(gridDebugObjectPrefab);

            gridSystemList.Add(gridSystem);
        }

        //Detect and set up the obstacles
        //If the raycast is shot inside the colider, it won't detect anything, thus we put a little offset
        float raycastOffsetDistance = 1f;
        for (int x = 0; x < width; x++)
        {
            for (int z = 0; z < height; z++)
            {
                for (int floor = 0; floor < floorAmount; floor++)
                {
                    GridPosition gridPosition = new GridPosition(x, z, floor);
                    Vector3 worldPosition = LevelGrid.Instance.GetWorldPosition(gridPosition);
                    GetPathNode(x, z, floor).SetIsWalkable(false);

                    if (Physics.Raycast(
                        worldPosition + Vector3.up * raycastOffsetDistance,
                        Vector3.down,
                        raycastOffsetDistance * 2,
                        floorLayerMask))
                    {
                        GetPathNode(x, z, floor).SetIsWalkable(true);
                    }

                    if (Physics.Raycast(
                        worldPosition + Vector3.down * raycastOffsetDistance,
                        Vector3.up,
                        raycastOffsetDistance * 2,
                        obstacleLayerMask))
                    {
                        GetPathNode(x, z, floor).SetIsWalkable(false);
                    }
                }
            }
        }

        pathFindingLinkList = new List<PathFindingLink>();
        foreach (Transform pathfindingLinkTransform in PathFindingLinkContainer)
        {
            if (pathfindingLinkTransform.TryGetComponent(out PathFindingLinkMonoBehaviour pathFindingLinkMonoBehaviour))
            {
                pathFindingLinkList.Add(pathFindingLinkMonoBehaviour.GetPathFindingLink());
            }
        }
    }

    //A* path finding algorithm
    public List<GridPosition> FindPath(GridPosition startGridPosition, GridPosition endGridPosition, out int pathLength)
    {
        List<PathNode> openList = new List<PathNode>();
        List<PathNode> closeList = new List<PathNode>();

        PathNode startNode = GetGridSystem(startGridPosition.floor).GetGridObject(startGridPosition);
        PathNode endNode = GetGridSystem(endGridPosition.floor).GetGridObject(endGridPosition);
        openList.Add(startNode);

        //Init all grid's path nodes for path finding
        for (int x = 0; x < width; x++)
        {
            for (int z = 0; z < height; z++)
            {
                for (int floor = 0; floor < floorAmount; floor++)
                {
                    GridPosition gridPosition = new GridPosition(x, z, floor);
                    PathNode pathNode = GetGridSystem(floor).GetGridObject(gridPosition);

                    pathNode.SetGCost(int.MaxValue);
                    pathNode.SetHCost(0);
                    pathNode.SetFCost();
                    pathNode.ResetCameFromPathNode();
                }
            }
        }

        startNode.SetGCost(0);
        startNode.SetHCost(CalculateDistance(startGridPosition, endGridPosition));
        startNode.SetFCost();

        //Search all the nodes
        while (openList.Count > 0)
        {
            PathNode currentNode = GetLowestFCostNode(openList);

            // Reached final node
            if(currentNode == endNode)
            {
                pathLength = endNode.GetFCost();
                return CalculatePath(endNode);
            }

            openList.Remove(currentNode);
            closeList.Add(currentNode);

            foreach (PathNode neighbourNode in GetNeighbourNodes(currentNode))
            {
                if (closeList.Contains(neighbourNode))
                {
                    continue;
                }

                if (!neighbourNode.IsWalkable())
                {
                    closeList.Add(neighbourNode);
                    continue;
                }

                int tentativeGCost = currentNode.GetGCost() + CalculateDistance(currentNode.GetGridPosition(), neighbourNode.GetGridPosition());

                if(tentativeGCost < neighbourNode.GetGCost())
                {
                    neighbourNode.SetCameFromPathNode(currentNode);
                    neighbourNode.SetGCost(tentativeGCost);
                    neighbourNode.SetHCost(CalculateDistance(neighbourNode.GetGridPosition(), endGridPosition));
                    neighbourNode.SetFCost();

                    if (!openList.Contains(neighbourNode))
                    {
                        openList.Add(neighbourNode);
                    }
                }
            }
        }

        //No path found
        pathLength = 0;
        return null;
    }

    //Get grid totalDistance with cost value between two grids object 
    public int CalculateDistance(GridPosition gridPositionA, GridPosition gridPositionB)
    {
        GridPosition gridPositionDistance = gridPositionA - gridPositionB;
        int xDistance = Mathf.Abs(gridPositionDistance.x);
        int zDistance = Mathf.Abs(gridPositionDistance.z);
        int remaining = Mathf.Abs(xDistance - zDistance);
        return MOVE_DIAGONAL_COST * Mathf.Min(xDistance, zDistance) + MOVE_STRAIGHT_COST * remaining;
    }
    
    //Search for the lowest cost of the nodes
    private PathNode GetLowestFCostNode(List<PathNode> pathNodes)
    {
        PathNode lowestFCostPathNode = pathNodes[0];
        for (int i = 0; i < pathNodes.Count; i++)
        {
            if (pathNodes[i].GetFCost() < lowestFCostPathNode.GetFCost())
            {
                lowestFCostPathNode = pathNodes[i];
            }
        }
        return lowestFCostPathNode;
    }

    private GridSystem<PathNode> GetGridSystem(int floor)
    {
        return gridSystemList[floor];
    }

    //Get path node
    private PathNode GetPathNode(int x, int z, int floor)
    {
        return GetGridSystem(floor).GetGridObject(new GridPosition(x, z, floor));
    }

    //Get Pathfinding's neighbour nodes, including above and below.
    private List<PathNode> GetNeighbourNodes(PathNode currentNode)
    {
        List<PathNode> neighbourNodes = new List<PathNode>();

        GridPosition gridPosition = currentNode.GetGridPosition();
        if(gridPosition.x - 1 >= 0)
        {
            //Left 
            neighbourNodes.Add(GetPathNode(gridPosition.x - 1, gridPosition.z + 0, gridPosition.floor));
            //Left Down
            if (gridPosition.z - 1 >= 0)
                neighbourNodes.Add(GetPathNode(gridPosition.x - 1, gridPosition.z - 1, gridPosition.floor));
            //Left Up
            if (gridPosition.z + 1 < height)
                neighbourNodes.Add(GetPathNode(gridPosition.x - 1, gridPosition.z + 1, gridPosition.floor));
        }
        if (gridPosition.x + 1 < width)
        {
            //Right 
            neighbourNodes.Add(GetPathNode(gridPosition.x + 1, gridPosition.z + 0, gridPosition.floor));
            //Right Down
            if(gridPosition.z - 1 >= 0)
                neighbourNodes.Add(GetPathNode(gridPosition.x + 1, gridPosition.z - 1, gridPosition.floor));
            //Right Up
            if(gridPosition.z + 1 < height)
                neighbourNodes.Add(GetPathNode(gridPosition.x + 1, gridPosition.z + 1, gridPosition.floor));
        }
        //Up
        if (gridPosition.z + 1 < height)
            neighbourNodes.Add(GetPathNode(gridPosition.x + 0, gridPosition.z + 1, gridPosition.floor));
        //Down
        if (gridPosition.z - 1 >= 0)
            neighbourNodes.Add(GetPathNode(gridPosition.x + 0, gridPosition.z - 1, gridPosition.floor));

        List<PathNode> totalNeighbourList = new List<PathNode>();
        totalNeighbourList.AddRange(neighbourNodes);

        //Search each neighbour node, see if there are nodes below or above it.
        /*foreach (PathNode pathNode in neighbourNodes)
        {
            GridPosition neighbourGridPosition = pathNode.GetGridPosition();
            //There is node below it
            if (neighbourGridPosition.floor - 1 >= 0)
            {
                totalNeighbourList.Add(GetPathNode(neighbourGridPosition.x, neighbourGridPosition.z, neighbourGridPosition.floor - 1));
            }
            //There is node above it
            if (neighbourGridPosition.floor + 1 < floorAmount)
            {
                totalNeighbourList.Add(GetPathNode(neighbourGridPosition.x, neighbourGridPosition.z, neighbourGridPosition.floor + 1));
            }

        }*/

        //Find if there are connected pathfinding link to other path node.
        List<GridPosition> pathfindingLinkGridPositionList = GetPathFindingLinkConnectedGridPositionList(gridPosition);
        foreach (GridPosition pathfindingLinkGridPosition in pathfindingLinkGridPositionList)
        {
            totalNeighbourList.Add(GetPathNode(pathfindingLinkGridPosition.x,
                                               pathfindingLinkGridPosition.z,
                                               pathfindingLinkGridPosition.floor
            ));
        }

        return totalNeighbourList;
    }

    //Search all pathfinding link list to find if given grid position has connected to other grid
    private List<GridPosition> GetPathFindingLinkConnectedGridPositionList(GridPosition gridPosition)
    {
        List<GridPosition> gridPositionList = new List<GridPosition>();

        foreach (PathFindingLink pathFindingLink in pathFindingLinkList)
        {
            if(pathFindingLink.gridPositionA == gridPosition)
            {
                gridPositionList.Add(pathFindingLink.gridPositionB);
            }
            if (pathFindingLink.gridPositionB == gridPosition)
            {
                gridPositionList.Add(pathFindingLink.gridPositionA);
            }
        }

        return gridPositionList;
    }

    //Get the path with grid position from start to end
    private List<GridPosition> CalculatePath(PathNode endNode)
    {
        List<PathNode> path = new List<PathNode>();
        path.Add(endNode);
        PathNode currentNode = endNode;
        while(currentNode.GetCameFromPathNode() != null)
        {
            path.Add(currentNode.GetCameFromPathNode());
            currentNode = currentNode.GetCameFromPathNode();
        }

        path.Reverse();

        List<GridPosition> gridPositions = new List<GridPosition>();
        foreach (PathNode pathNode in path)
        {
            gridPositions.Add(pathNode.GetGridPosition());
        }

        return gridPositions;
    }

    //Return wheater the grid-position is walkable or not
    public bool IsWalkableGridPosition(GridPosition gridPosition)
    {
        return GetGridSystem(gridPosition.floor).GetGridObject(gridPosition).IsWalkable();
    }


    //Set the grid-position wheater is walkable or not
    public void SetIsWalkableGridPosition(GridPosition gridPosition, bool isWalkable)
    {
        GetGridSystem(gridPosition.floor).GetGridObject(gridPosition).SetIsWalkable(isWalkable);
    }

    //Return wheater there is a path to the specific grid position or not
    public bool HasPath(GridPosition startGridPosition, GridPosition endGridPosition)
    {
        return FindPath(startGridPosition, endGridPosition, out int pathLength) != null;
    }

    //Simply return the path length that path finding found
    public int GetPathLength(GridPosition startGridPosition, GridPosition endGridPosition)
    {
        FindPath(startGridPosition, endGridPosition, out int pathLength);
        return pathLength;
    }
}
