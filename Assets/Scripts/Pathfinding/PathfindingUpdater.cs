using System;
using UnityEngine;

//This scripts listen to the event and update path finder when the event has invoked
public class PathfindingUpdater : MonoBehaviour
{
    private void Start()
    {
        DestructableObject.OnAnyDestroyed += Destructable_OnAnyDestroyed;
    }

    private void Destructable_OnAnyDestroyed(object sender, EventArgs e)
    {
        DestructableObject destructableObject = sender as DestructableObject;
        PathFinding.Instance.SetIsWalkableGridPosition(destructableObject.GetGridPosition(), true);
        if(destructableObject is ICoverable)
            LevelGrid.Instance.RemoveCoverableAtGridPosition(destructableObject.GetGridPosition());
    }
}
