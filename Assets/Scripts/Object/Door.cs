using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Device;

public class Door : MonoBehaviour, IInteractable, IAudioable
{
    public static event EventHandler OnAnyDoorOpened;
    public event EventHandler OnDoorOpened;

    [SerializeField] private bool isOpen;
    [SerializeField] private Transform leftDoor;
    [SerializeField] private Transform rightDoor;
    [SerializeField] private Vector3 offset;
    [SerializeField] private Material indicatorOnMaterial;
    [SerializeField] private Material indicatorOffMaterial;
    [SerializeField] private MeshRenderer meshRenderer;

    [SerializeField] private List<Sound> sounds;
    [SerializeField] private AudioSource audioSource;

    private GridPosition gridPosition;
    private GridPosition gridPositionLeftDoor;
    private GridPosition gridPositionRightDoor;
    private Animator animator;
    private Action onInteractComplete;
    private bool isActive;
    private float timer;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    private void Start()
    {
        gridPosition = LevelGrid.Instance.GetGridPosition(transform.position);
        gridPositionLeftDoor = LevelGrid.Instance.GetGridPosition(leftDoor.position + offset*1.01f);
        gridPositionRightDoor = LevelGrid.Instance.GetGridPosition(rightDoor.position - offset);
        LevelGrid.Instance.SetInteractableAtGridPosition(gridPosition, this);

        animator.speed = 100;

        if (isOpen)
        {
            SetIndicatorOn();
            PathFinding.Instance.SetIsWalkableGridPosition(gridPositionLeftDoor, true);
            PathFinding.Instance.SetIsWalkableGridPosition(gridPositionRightDoor, true);
        }
        else
        {
            SetIndicatorOff();
            PathFinding.Instance.SetIsWalkableGridPosition(gridPositionLeftDoor, false);
            PathFinding.Instance.SetIsWalkableGridPosition(gridPositionRightDoor, false);
        }
    }

    private void Update()
    {
        if (!isActive)
        {
            return;
        }

        timer -= Time.deltaTime;

        if(timer <= 0f)
        {
            isActive = false;
            onInteractComplete();
        }
    }

    public void Interact(Action onInteractionComplete)
    {
        this.onInteractComplete = onInteractionComplete;
        isActive = true;
        timer = 1f;
        if (isOpen)
        {
            Close();
        }
        else
        {
            Open();
        }
    }

    private void Open()
    {
        isOpen = true;
        SetIndicatorOn();
        animator.speed = 1;
        animator.SetBool("IsOpen", isOpen);
        Play("Door Open");
        PathFinding.Instance.SetIsWalkableGridPosition(gridPositionLeftDoor, true);
        PathFinding.Instance.SetIsWalkableGridPosition(gridPositionRightDoor, true);
        OnAnyDoorOpened?.Invoke(this, EventArgs.Empty);
        OnDoorOpened?.Invoke(this, EventArgs.Empty);
    }

    private void Close()
    {
        isOpen = false;
        SetIndicatorOff();
        animator.speed = 1;
        animator.SetBool("IsOpen", isOpen);
        Play("Door Close");
        PathFinding.Instance.SetIsWalkableGridPosition(gridPositionLeftDoor, false);
        PathFinding.Instance.SetIsWalkableGridPosition(gridPositionRightDoor, false);
    }

    private void SetIndicatorOn()
    {
        meshRenderer.material = indicatorOnMaterial;
    }

    private void SetIndicatorOff()
    {
        meshRenderer.material = indicatorOffMaterial;
    }


    //
    //
    //Audio Interface
    //No loops, No packs, per object, 3D sound
    public void AudioSourceInit()
    {
        return;
    }

    public void Play(string name, bool loop = false)
    {
        Sound sound = sounds.Find(sound => sound.name == name);
        if (sound == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }

        PlayAudio(sound);
    }

    private void PlayAudio(Sound sound)
    {
        audioSource.volume = sound.volume;
        audioSource.pitch = sound.pitch;
        audioSource.clip = sound.clip;
        audioSource.Play();
    }

    public void Stop(string name, bool stopImediate = false)
    {
        return;
    }
}
