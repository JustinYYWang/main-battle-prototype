using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructableObject : MonoBehaviour, IHitable
{
    public static event EventHandler OnAnyDestroyed;

    protected GridPosition gridPosition;

    protected virtual void Start()
    {
        gridPosition = LevelGrid.Instance.GetGridPosition(this.transform.position);
    }

    //Return this object's grid-position
    public GridPosition GetGridPosition()
    {
        return gridPosition;
    }

    public Vector3 GetWorldPosition()
    {
        return transform.position;
    }

    //It broke when it has been touched
    public virtual void Damage(float damageAmount)
    {
        Debug.Log("Coverable Destroy");
        OnAnyDestroyed?.Invoke(this, EventArgs.Empty);
    }

    //Create a more dramatic object destroyed scene
    protected virtual void ApplyExplosionToWreckage(Transform root, float explosionForce, Vector3 explosionPosition, float explosionRadius)
    {
        foreach (Transform child in root)
        {
            if (child.TryGetComponent<Rigidbody>(out Rigidbody childRigidBody))
            {
                childRigidBody.AddExplosionForce(explosionForce, explosionPosition, explosionRadius);
            }

            ApplyExplosionToWreckage(child, explosionForce, explosionPosition, explosionRadius);
        }
    }
}
