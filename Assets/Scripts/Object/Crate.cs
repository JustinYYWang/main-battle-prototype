using Unity.VisualScripting.Antlr3.Runtime;
using UnityEngine;

public class Crate : DestructableObject, ICoverable
{
    [SerializeField] private Transform wreckagePrefab;
    [SerializeField] private CoverType cover;

    CoverType ICoverable.cover { get => cover; }

    GridPosition ICoverable.gridPosition { get => gridPosition; }

    protected override void Start()
    {
        base.Start();
        LevelGrid.Instance.SetCoverableAtGridPosition(gridPosition, this);
    }

    public override void Damage(float damageAmount)
    {
        Transform wreckageTransform = Instantiate(wreckagePrefab, transform.position, Quaternion.identity);

        ApplyExplosionToWreckage(wreckageTransform, 150f, transform.position, 10f);
        Destroy(gameObject);

        base.Damage(damageAmount);
    }
}
