
public interface ICoverable
{
    CoverType cover { get; }
    GridPosition gridPosition { get; }
}

public enum CoverType
{
    None,
    One_Quarter,
    Half,
    Three_Quarter,
    Full
}
