//Different type of damage?
using UnityEngine;

public interface IHitable
{
    public void Damage(float damageAmount);

    public GridPosition GetGridPosition();

    public Vector3 GetWorldPosition();
}
