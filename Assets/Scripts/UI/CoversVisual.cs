using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class CoversVisual : MonoBehaviour
{
    [SerializeField] private Texture fullTexture;
    [SerializeField] private Texture three_quarterTexture;
    [SerializeField] private Texture halfTexture;
    [SerializeField] private GameObject cover_visuals;

    private Texture coverTexture;

    private CoverType coverType;


    private void Start()
    {
        coverType = GetComponent<ICoverable>().cover;

        switch (coverType)
        {
            case CoverType.Full:
                coverTexture = fullTexture;
                break;
            case CoverType.Three_Quarter:
                coverTexture = three_quarterTexture;
                break;
            case CoverType.Half:
                coverTexture = halfTexture;
                break;
        }

        for (int i = 0; i < cover_visuals.transform.childCount; i++)
        {
            GameObject child = cover_visuals.transform.GetChild(i).gameObject;
            child.GetComponent<Renderer>().material.SetTexture("_BaseMap", coverTexture);
        }
    }

    void OnMouseEnter()
    {
        cover_visuals.SetActive(true);
    }

    void OnMouseExit()
    {
        cover_visuals.SetActive(false);
    }
}
