# Main Battle Prototype

![image](https://gitlab.com/JustinYYWang/main-battle-prototype/-/raw/main/Public/Shooting.gif)

**目前最新的版本有問題**，若要正式運行請使用 **V_0.0.5.1** 版本

## Description

Main Battle Prototype 是 Sistine Spectrum 使用 Unity 建構一個簡易的類 XCOM 類型的遊戲原型，主要是測試我們發想的遊戲機制是否有趣，**這是一個尚未完成的** **Prototype** 有許多地方的設計都不完善，但是作為類 XCOM 遊戲的基本機制都已經開發完成。此 repository 主要用來內部組織的程式版本控制。

![image](https://gitlab.com/JustinYYWang/main-battle-prototype/-/raw/main/Public/Door.gif)
角色與物品互動 - 門

基本上遊戲的邏輯有三大塊：

1. Grids - 將網格邏輯應用到 Unity 3D 世界中，並且每個網格必須能儲存以及修改資訊以利單位或物品操作，讓系統控制邏輯演算。並以 A* 尋路演算法讓單位能在多層樓的網格上移動。
![image](https://gitlab.com/JustinYYWang/main-battle-prototype/-/raw/main/Public/Multti-Floor.gif)
多階層網格系統

2. Units and Objects - 像單位射擊、投擲手榴彈、移動、物品毀損、門的互動等等邏輯實施，以及相對應的動畫搭配與SFX。
![image](https://gitlab.com/JustinYYWang/main-battle-prototype/-/raw/main/Public/Grenade.gif)
手榴彈與預測線系統

3. UI - 將遊戲的資訊展示與玩家輸入輸出介面控制等等 UI 設計與邏輯。

使用的 Materials, Shaders, Textures 是在 Unity Asset Store 上購買由 Synty Studios 出品的 POLYGON Prototype 套件。

此 XCOM 遊戲的 Prototype 是修改自 Hugo Cardoso 的[網路課程](https://www.gamedev.tv/dashboard/courses/26)

## Installation
整個專案都是使用 Unity 的專案架構，依照 Unity 官網建議只版控需納入版控部分，主要是變動成份較大的，例如 Assets, Packages 與 ProjectSettings

## Usage (Constructing)

將整包 git 拉下來後，利用 Unity Hub 的開啟專案將目的資料夾打開。
在 Asset 的 Scene 資料夾有 Game Scene ，此為預設 Prototype 關卡（尚未設計完成）。
這包版本是用 Unity 6.0.0

## Support

若在執行時遇到任何問題，請透過 e-mail 聯絡原作者（Justin Wang - justin17728@gmail.com）


## Roadmap

- [X] 類XCOM基本遊戲架構建制
    - [X] 角色移動與選擇
    - [X] 網格系統與攝影機
    - [X] 角色行動
    - [X] 敵方角色與戰鬥系統
    - [X] A* 尋路演算法
- [X] UI/UX
    - [X] 基本UI, 血條、掩護提示、行動、回合結束等等
    - [X] 攻擊範圍提示、手榴彈投擲預測線等等
    - [X] 音樂應用：BGM, SFX
- [ ] 遊戲設計與美術應用
    - [ ] 遊戲機制
    - [ ] 美術工程
    - [ ] 依需求重新設計遊戲系統

## Known Issues
1. 手榴彈的爆炸判定 - 當手榴彈爆炸時範圍判定會有角色明明在障礙物後方但還是被炸到的判定，所以先改為以網格運算。但這樣系統要 Handle 的狀況太多容易會有沒處理的情況導致機制判定方面的 Bug 。 因此最後還是要改回來，初步判斷是 Unity 在判斷 Collision 時若是從 Collision 裡面 ray cast 出來會漏判 Collision
2. A* 需要優化，目前多層樓的方式就只是單純的多一維（變成三維） array，浪費了許多沒用到的網格而白白佔記憶體
3. 門打不開
4. 手榴彈將障礙物炸碎後，系統依舊判定障礙物存在

## Contributing
這是一個私人專案，專為組織 Sistine Spectrum 使用。若任何與以上相關人士對此也有興趣請透過 e-mail 聯絡我（Justin Wang - justin17728@gmail.com），我們可以一起論如何使這個專案完善。
若想要了解建構這個所需的知識，建議可以從下列有極大幫助的網站開始：
1. [CodeMonkey](https://www.youtube.com/watch?v=ezlkGhFBrmg&ab_channel=CodeMonkey)
2. [Projectile Trajectory Predictor - 1 ](https://www.youtube.com/watch?v=p8e4Kpl9b28&t=439s&ab_channel=Tarodev)
3. [Projectile Trajectory Predictor - 2](https://www.youtube.com/watch?v=rgJGkYM2_6E&ab_channel=ForlornU)
4. [POLYGON Prototype](https://assetstore.unity.com/packages/essentials/tutorial-projects/polygon-prototype-low-poly-3d-art-by-synty-137126)
5. [Brackey - Audio](https://www.youtube.com/watch?v=6OT43pvUyfY)

## Authors and acknowledgment
這個專案是由 Sistine Spectrum 主持與建構。

## License
For open source projects, say how it is licensed.
